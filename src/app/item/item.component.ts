import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { ItemService } from './item.service';
import { HomeService } from '../home/home.service';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';

declare var $: any;
declare var google: any;
declare var FB: any;
declare var moment: any;


declare interface Window {
  adsbygoogle: any[];
}
declare var adsbygoogle: any[];

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  constructor(
    private _missionService: MissionService,
    private _meta: Meta,
    private _conf: Configuration,
    private route: ActivatedRoute,
    private _router: Router,
    private homeService: HomeService,
    private _service: ItemService,
    private _lang: LanguageService) { }

  headerOpen: string;
  postId: any;
  languageCode:string = 'rs';
  itemList: any;
  allListYelo = false;
  token: any;
  valueToshow = '';
  valueArr =[];
  currentUrlLocation:string ='';
  followers: any;
  followOpen = false;
  loginUsername: any;
  otherOffer: any;
  offerListLength: any;
  reportList: any;
  reportReasonList: any;
  reportDes: any;
  reportDescription: any;
  reportErr: any;
  offset = 40;
  limit = 0;
  willing = 0;
  makeOfferPrice: string;
  offerSent = false;
  lengthTextarea: any;
  listComments = false;
  commentMsg: any;
  textAreaMsg: any;
  allComment = false;
  sellEmail: string;
  sellPhone: string;
  categoryId : any;
  email_Error = false;
  phone_Error = false;
  registerSave = false;
  PEerror: any;
  succesSeller = false;
  howPopupSuccess = false;
  postListImg = false;
  listSell: any;
  item: any
  successReport: any;
  postEdit: any;
  catList: any;
  newLineDesc: any =[];
  loaderButton = false;
  errMsg: any;
  ShowCopyUrl:boolean = false;
  editedEurCurrency:any;
  cloudinaryOptions: CloudinaryOptions = new CloudinaryOptions({
    cloudName: 'yeloadmin',
    uploadPreset: 'iirvrtog',
    autoUpload: true
  });
  constPrice:any;

  uploader: CloudinaryUploader = new CloudinaryUploader(this.cloudinaryOptions);

  imageUploader = (item: any, response: string, status: number, headers: any) => {
    let cloudinaryImage = JSON.parse(response);
    console.log(cloudinaryImage)
    let list = {
      img: cloudinaryImage.secure_url,
      cloudinaryPublicId: cloudinaryImage.public_id,
      containerHeight: cloudinaryImage.height,
      containerWidth: cloudinaryImage.width
    }
    this.submitData(list);
  };

  ngOnInit() {
    // let t = "pqpq↵qkwlq↵sdkjsdkj";
    // let split = t.split("↵");
    //         console.log("the description is :", split);
    this.uploader.onSuccessItem = this.imageUploader;
    this.currentUrlLocation = '';
    this.ShowCopyUrl = false;
    // this.item = this._lang.engItem;
    // let selectedLang  = Number(this._conf.getItem("Language"));
    let selectedLang  = Number(sessionStorage.getItem("Language"));
    if(selectedLang){
      switch(selectedLang){

        case 1: this.item = this._lang.engItem1;
                this.languageCode= 'sr';
                break;
        case 2: this.item = this._lang.engItem1;
                this.languageCode= 'srp';
                break;

        case 3: this.item = this._lang.engItem;
                this.languageCode= 'en';
                break;
      }
    }
    else{
      this.item = this._lang.engItem1;
      this.languageCode = 'sr';
    }

    $(window).on('popstate', function () {
      $('.modal').modal('hide');
    });
    this._missionService.confirmheaderOpen(this.headerOpen);
    FB.init({
      appId: '895123320630756',
      cookie: false,
      xfbml: true,
      version: 'v2.7'
    });
    this.token = this._conf.getItem('authToken');
    this.loginUsername = this._conf.getItem('username');

    this.route.params.subscribe(params => {
      if (params['id'] == '1510560388842') {
        var script = document.createElement("script");
        script.src = "https://d2qb2fbt5wuzik.cloudfront.net/yelowebsite/js/rating.json";
        script.type = "application/ld+json";
        document.body.appendChild(script);
      }
      this.postId = params['id'];
      if (this.postId && !this.token) {
        let Id = {
          postId: this.postId,
          languageCode: this.languageCode
        }
        this._service.guestPostList(Id)
          .subscribe((res) => {
            this.allListYelo = true;
            this.itemList = res.data[0];
            this.editedEurCurrency = this.addCommaToPrice(this.itemList.price);
            let t = this.itemList.description;
            this.constPrice = this.itemList.price;
            if(t != null && t.indexOf('\n') > 0){
              this.newLineDesc = t.split('\n');
              this.itemList.description  = t;
          }
            this._meta.addTag({ name: 'keywords', content: this.itemList.seoKeyword })
            this._meta.addTag({ name: 'description', content: this.itemList.seoDescription })
            this._meta.addTag({ name: 'title', content: this.itemList.seoTitle })
            this._meta.addTag({ name: 'og:keywords', content: this.itemList.seoKeyword })
            this._meta.addTag({ name: 'og:description', content: this.itemList.seoDescription })
            this._meta.addTag({ name: 'og:title', content: this.itemList.seoTitle })
            this._meta.addTag({ name: 'og:image', content: this.itemList.mainUrl })
            this.hashtagFunction();
            this.timeFunction();
            this.locationMap();
            this.myOtherGuestOffers();
            if (res.code == 204) {
              this.postListImg = true;
            } else {
              this.postListImg = false;
            }
          });
      } else {
        let list = {
          token: this.token,
          postId: this.postId,
          latitude: "12.972442",
          longitude: "77.580643",
          city: "bengaluru",
          countrySname: "in",
          languageCode: this.languageCode
        }
        list.latitude = this._conf.getItem('lat');
        list.longitude =this._conf.getItem('lng');
        list.city = this._conf.getItem('city');
        list.countrySname = this._conf.getItem('country');
        // $.get("https://ipapi.co/json", (res)=> {
        //   console.log("the response ",res)
              // let ipList = res.loc.split(",");
              // this._conf.getItem('city');
              // this._conf.getItem('country');
              // this._conf.getItem('lat');
              // this._conf.getItem('lng');
              list.latitude = this._conf.getItem('lat');
              list.longitude =this._conf.getItem('lng');
              list.city = this._conf.getItem('city');
              list.countrySname = this._conf.getItem('country');
        // });
        console.log(list);
        this.homeService.getIpAddress()
          .subscribe((res) => {
            console.log(res)
            // let ipList = res.loc.split(",");
            list.latitude = res.latitude;
            list.longitude = res.longitude;
            list.city = res.city;
            list.countrySname = res.country;
          })
        console.log(list);
        this._service.userPostList(list)
          .subscribe((res) => {
            this.allListYelo = true;
            if (res.code == 200) {
              this.itemList = res.data[0];
              this.constPrice = this.itemList.price;
              // this.editedEurCurrency = this.itemList.price;
              this.editedEurCurrency = this.addCommaToPrice(this.itemList.price);
              let t = this.itemList.description;
              if(t != null && t.indexOf('\n') > 0){
                this.newLineDesc = t.split('\n');
                this.itemList.description  = t;
              }
              this._meta.addTag({ name: 'keywords', content: this.itemList.seoKeyword })
              this._meta.addTag({ name: 'description', content: this.itemList.seoDescription })
              this._meta.addTag({ name: 'title', content: this.itemList.seoTitle })
              this._meta.addTag({ name: 'og:keywords', content: this.itemList.seoKeyword })
              this._meta.addTag({ name: 'og:description', content: this.itemList.seoDescription })
              this._meta.addTag({ name: 'og:title', content: this.itemList.seoTitle })
              this._meta.addTag({ name: 'og:image', content: this.itemList.mainUrl })
              this.hashtagFunction();
              this.timeFunction();
              if (this.itemList.commentData.length > 0) {
                this.commentTimeFunction();
              }
              this.locationMap();
              this.getFollowers(this.itemList);
              this.myOtherUserOffers();
            }
            if (res.code == 204) {
              this._router.navigate([""]);
            }
          });
      }
    });
    this.listGetCategories();
  }



  ngAfterViewInit() {
    // setTimeout(() => {
    //   try {
    //     (window['adsbygoogle'] = window['adsbygoogle'] || []).push({});
    //   } catch (e) {
    //     //  alert(e);
    //     console.error("error", e);
    //   }
    // }, 2000);
  }


  commentTimeFunction() {
    var len = this.itemList.commentData;
    for (var i = 0; i < len.length; i++) {
      var post = this.itemList.commentData[i].commentedOn;
      var poston = Number(post);
      var postdate = new Date(poston);
      var currentdate = new Date();
      var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
      var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);

      if (1 < diffDays) {
        this.itemList.commentData[i].commentedOn = diffDays + "d";
      } else if (1 <= diffHrs) {
        this.itemList.commentData[i].commentedOn = diffHrs + "h";
      } else {
        this.itemList.commentData[i].commentedOn = diffMins + "m";
      }
      // console.log(this.itemList.commentData[i].commentedOn);
      // this.itemList.postedOn = postedOn;
    }
  }

  timeFunction() {
    // var post = this.itemList.postedOn;
    // var poston = Number(post);
    // var postdate = new Date(poston);
    // var currentdate = new Date();
    // var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
    // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    // var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
    // var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);
    var postOd = Math.floor(this.itemList.postedOn/1000);
    var start = moment.unix(postOd).format('MMM DD YYYY hh:mm:ss A')
    var end = moment(new Date(), 'MMM DD YYYY hh:mm:ss A').format('MMM DD YYYY hh:mm:ss A');
    console.log('the duration is start date :', start)
    console.log('the duration is end date :', end)
    start = moment(start,'MMM DD YYYY hh:mm:ss A');
    end = moment(end,'MMM DD YYYY hh:mm:ss A');
    var diffDays = moment.duration(end.diff(start)).asDays();
    var diffHrs = moment.duration(end.diff(start)).asHours();
    var diffMins = moment.duration(end.diff(start)).asMinutes();
    console.log('the duration is :', diffDays,diffHrs,diffMins)
    var postedOn;
    var days ;
    if (1 < diffDays) {
    (this.languageCode == 'sr' || this.languageCode == 'srp') ? days=' dana' : days= ' days';
    postedOn = Math.floor(diffDays) +days;
    } else if (1 <= diffHrs) {
      (this.languageCode == 'sr' || this.languageCode == 'srp') ? days=' sati' : days= ' hours';
      postedOn = Math.floor(diffHrs) + days;
    } else {
      (this.languageCode == 'sr' || this.languageCode == 'srp') ? days=' minuta' : days= ' minutes';
      postedOn = Math.floor(diffMins) + days;
    }
    this.itemList.postedOn = postedOn;
    console.log('the posted on:',this.itemList.postedOn,postedOn )
  }

  public currencyList = [
    { value: "EUR", text:"montenegro" },
    { value: "DIN", text:"Serbian dinar"}
    // { value: "USD", text: "United States Dollars" },
    // { value: "EUR", text: "Euro" },
    // { value: "GBP", text: "United Kingdom Pounds" },
    // { value: "DZD", text: "Algeria Dinars" },
    // { value: "ARP", text: "Argentina Pesos" },
    // { value: "AUD", text: "Australia Dollars" },
    // { value: "ATS", text: "Austria Schillings" },
    // { value: "BSD", text: "Bahamas Dollars" },
    // { value: "BBD", text: "Barbados Dollars" },
    // { value: "BEF", text: "Belgium Francs" },
    // { value: "BMD", text: "Bermuda Dollars" },
    // { value: "BRR", text: "Brazil Real" },
    // { value: "BGL", text: "Bulgaria Lev" },
    // { value: "CAD", text: "Canada Dollars" },
    // { value: "CLP", text: "Chile Pesos" },
    // { value: "CNY", text: "China Yuan Renmimbi" },
    // { value: "CYP", text: "Cyprus Pounds" },
    // { value: "CSK", text: "Czech Republic Koruna" },
    // { value: "DKK", text: "Denmark Kroner" },
    // { value: "NLG", text: "Dutch Guilders" },
    // { value: "XCD", text: "Eastern Caribbean Dollars" },
    // { value: "EGP", text: "Egypt Pounds" },
    // { value: "FJD", text: "Fiji Dollars" },
    // { value: "FIM", text: "Finland Markka" },
    // { value: "FRF", text: "France Francs" },
    // { value: "DEM", text: "Germany Deutsche Marks" },
    // { value: "XAU", text: "Gold Ounces" },
    // { value: "GRD", text: "Greece Drachmas" },
    // { value: "HKD", text: "Hong Kong Dollars" },
    // { value: "HUF", text: "Hungary Forint" },
    // { value: "ISK", text: "Iceland Krona" },
    // { value: "INR", text: "India Rupees" },
    // { value: "IDR", text: "Indonesia Rupiah" },
    // { value: "IEP", text: "Ireland Punt" },
    // { value: "ILS", text: "Israel New Shekels" },
    // { value: "ITL", text: "Italy Lira" },
    // { value: "JMD", text: "Jamaica Dollars" },
    // { value: "JPY", text: "Japan Yen" },
    // { value: "JOD", text: "Jordan Dinar" },
    // { value: "KRW", text: "Korea(South) Won" },
    // { value: "LBP", text: "Lebanon Pounds" },
    // { value: "LUF", text: "Luxembourg Francs" },
    // { value: "MYR", text: "Malaysia Ringgit" },
    // { value: "MXP", text: "Mexico Pesos" },
    // { value: "NLG", text: "Netherlands Guilders" },
    // { value: "NZD", text: "New Zealand Dollars" },
    // { value: "NOK", text: "Norway Kroner" },
    // { value: "PKR", text: "Pakistan Rupees" },
    // { value: "XPD", text: "Palladium Ounces" },
    // { value: "PHP", text: "Philippines Pesos" },
    // { value: "XPT", text: "Platinum Ounces" },
    // { value: "PLZ", text: "Poland Zloty" },
    // { value: "PTE", text: "Portugal Escudo" },
    // { value: "ROL", text: "Romania Leu" },
    // { value: "RUR", text: "Russia Rubles" },
    // { value: "SAR", text: "Saudi Arabia Riyal" },
    // { value: "XAG", text: "Silver Ounces" },
    // { value: "SGD", text: "Singapore Dollars" },
    // { value: "SKK", text: "Slovakia Koruna" },
    // { value: "ZAR", text: "South Africa Rand" },
    // { value: "KRW", text: "South Korea Won" },
    // { value: "ESP", text: "Spain Pesetas" },
    // { value: "XDR", text: "Special Drawing Right(IMF)" },
    // { value: "SDD", text: "Sudan Dinar" },
    // { value: "SEK", text: "Sweden Krona" },
    // { value: "CHF", text: "Switzerland Francs" },
    // { value: "TWD", text: "Taiwan Dollars" },
    // { value: "THB", text: "Thailand Baht" },
    // { value: "TTD", text: "Trinidad and Tobago Dollars" },
    // { value: "TRL", text: "Turkey Lira" },
    // { value: "VEB", text: "Venezuela Bolivar" },
    // { value: "ZMK", text: "Zambia Kwacha" },
    // { value: "EUR", text: "Euro" },
    // { value: "XCD", text: "Eastern Caribbean Dollars" },
    // { value: "XDR", text: "Special Drawing Right(IMF)" },
    // { value: "XAG", text: "Silver Ounces" },
    // { value: "XAU", text: "Gold Ounces" },
    // { value: "XPD", text: "Palladium Ounces" },
    // { value: "XPT", text: "Platinum Ounces" }
  ]


  myOtherGuestOffers() {
    let list = {
      postId: this.postId,
      membername: this.itemList.membername,
      latitude: this.itemList.latitude,
      longitude: this.itemList.longitude,
      offset: this.offset,
      limit: this.limit
    }

    this._service.otherOffersGuestPostList(list)
      .subscribe((res) => {
        this.otherOffer = res.data;
        if (this.otherOffer && this.otherOffer.length > 0) {
          this.offerListLength = true;
        } else {
          this.offerListLength = false;
        }
      });
  }


  myOtherUserOffers() {
    let list = {
      token: this.token,
      postId: this.postId,
      membername: this.itemList.membername,
      latitude: this.itemList.latitude,
      longitude: this.itemList.longitude,
      offset: this.offset,
      limit: this.limit
    }

    this._service.otherOffersUserPostList(list)
      .subscribe((res) => {
        this.otherOffer = res.data;
        if (this.otherOffer && this.otherOffer.length > 0) {
          this.offerListLength = true;
        } else {
          this.offerListLength = false;
        }
      });
  }

  watchList(val, label) {
    if (this.token) {
      let list = {
        token: this.token,
        postId: this.postId,
        label: 'Photo'
      }
      if (label == 1) {
        list.label = 'Video';
      }
      if (val == 0) {
        this.itemList.likeStatus = 1;
        this._service.likePost(list)
          .subscribe((res) => {
          });
      } else {
        this.itemList.likeStatus = 0;
        this._service.unlikePost(list)
          .subscribe((res) => {
          });
      }
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }

  searchCategory(val) {
    // this._conf.setItem('categoryName', val.toLowerCase());
    // this._router.navigate(["./search"]);
  }

  itemDetails(id) {
    // $("html, body").animate({ scrollTop: 0 }, 500);
    this._router.navigate(["./item", id]);
    $("html, body").animate({ scrollTop: 0 }, 500);
  }

  postTextArea(val) {
    this.lengthTextarea = val.length;
  }


  changeImage(val) {
    console.log("the value of image is :", val)
    $(".imgItem").removeClass("active");
    $(".item").removeClass("active");
    $("#" + val + "img").addClass("active");
    $("#" + val).addClass("active");
  }

  InnerCarousel() {
    setTimeout(() => {
      var idx = $('#myCarousel').find('.active').attr("id");
      $(".imgItem").removeClass("active");
      $("#" + idx + "img").addClass("active");
    }, 650);
  }

  getFollowers(name) {
    let list = {
      token: this.token,
      membername: name.membername,
      offset: 0,
      limit: 40
    }
    this._service.getFollowersPostList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.followers = res.memberFollowers;
          if (this.followers && this.followers.length > 0) {
            this.followOpen = true;
          } else {
            this.followOpen = false;
          }
        } else {
          this.followOpen = false;
        }
      });

  }

  memberProfile(val) {
    // let memberName = val.username;
    $(".modal").modal("hide");
    if (this.token) {
      if (this.loginUsername != val) {
        this._router.navigate(["p", val]);
      } else {
        this._router.navigate(["./settings"]);
      }
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }

  followerButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        userNameToFollow: val
      }
      if (i == undefined) {
        this.itemList.followRequestStatus = 1;
      } else {
        this.followers[i].userFollowRequestStatus = 1;
      }
      this._service.followersPostList(list)
        .subscribe((res) => {
          if (res.code == 200) {
            $("#followItemPopup").modal("show");
          }
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }

  }

  unFollowButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        unfollowUserName: val
      }
      if (i == undefined) {
        this.itemList.followRequestStatus = null;
      } else {
        this.followers[i].userFollowRequestStatus = null;
      }
      this._service.unfollowPostList(list)
        .subscribe((res) => {

        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }

  }
  EurCurrency:any ='';
  makeOfferPriceEmpty() {
    this.makeOfferPrice = "";
    this.offerSent = false;
    this.EurCurrency = this.addCommaToPrice(this.itemList.price);
    this.editedEurCurrency = this.addCommaToPrice(this.itemList.price);
    // if(this.editedEurCurrency.lastIndexof(',') > -1){
    //   this.editedEurCurrency = this.itemList.price.toFixed(2).toString().replace(',','.');  
    // }
  }

  makeOffer(item) {
    let mqttId = this._conf.getItem('mqttId');
    let username = this._conf.getItem('username');
    let profilePicUrl = this._conf.getItem('profilePicUrl');
    let price ;
    if (!this.makeOfferPrice) {
      this.itemList.negotiable == 0 ? this.makeOfferPrice = this.editedEurCurrency : this.makeOfferPrice = this.EurCurrency;
      console.log("the makeOffer price is:", this.makeOfferPrice);
      this.makeOfferPrice.lastIndexOf(',') <= -1 ?
      price = this.makeOfferPrice.split('.').join('') : price = this.makeOfferPrice.split('.').join('').split(',').join('.');
        // price = this.makeOfferPrice.replace('.','') : price = this.makeOfferPrice.replace('.','').replace(',','.');
        console.log("the price is:",price,this.makeOfferPrice.lastIndexOf(',') )
    if(this.makeOfferPrice.lastIndexOf(',') > -1){
      this.makeOfferPrice = (this.makeOfferPrice.split(',').join('.'));
        // this.makeOfferPrice = (this.makeOfferPrice.replace(',','.'));
      }
    }

    if (this.makeOfferPrice) {
      if (this.token && mqttId) {
        var list = {
          token: this.token,
          postId: this.postId,
          offerStatus: '1',
          price: (this.makeOfferPrice),
          type: String(item.postType),
          membername: item.membername,
          sendchat: null
        }
        var today = new Date();
        var formattedtoday = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var date = moment(formattedtoday).valueOf();

        let itemList = {
          dataSize: "1",
          from: mqttId,
          id: String(date),
          isSold: "0",
          name: username,
          offerType: "1",
          payload: btoa(this.makeOfferPrice),
          productId: String(item.postId),
          productImage: item.mainUrl,
          productName: item.productName,
          productPrice: item.price,
          secretId: String(item.postId),
          thumbnail: item.thumbnailImageUrl,
          to: item.memberMqttId,
          toDocId: String(date),
          type: String(15),
          userImage: profilePicUrl
        }
        list.sendchat = itemList;
        console.log("the makeoffer date :",list);
        this._service.makeOfferPostList(list)
          .subscribe((res) => {
            if (res.code == 200) {
              this.offerSent = true;
              $("#makeOffer").modal("hide");
              this.successReport = "Offer Sent";
              $("#reportItemPopup").modal("show");
              setTimeout(() => {
                $(".modal").modal("hide");
              }, 2000);
            }
          });
      } else {
        $("#makeOffer").modal("hide");
        this._missionService.confirmLogin(this.headerOpen);
      }
    }
  }

  reportItem() {
    if (this.token) {
      let list = {
        token: this.token
      }
      this._service.reportPostList(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.reportList = res.data;
            this.reportDes = true;
            this.successReport = "Reported";
            $("#reportItemPopup").modal("show");
          }
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
      // this._missionService.confirmLogin(this.headerOpen);
    }
  }

  reportReason(report) {
    this.reportDescription = "";
    this.reportErr = "";
    this.reportDes = false;
    this.reportReasonList = report;
  }

  reportPostItem() {
    let list = {
      token: this.token,
      membername: this.itemList.membername,
      reasonId: this.reportReasonList._id,
      description: this.reportDescription,
      postId: this.postId
    }
    this._service.reportPosting(list)
      .subscribe((res) => {
        this.reportDes = false;
        this.reportList = false;
        this.reportErr = res.messsage;
        if (res.code == 200) {
          setTimeout(() => {
            $(".modal").modal("hide");
          }, 1000);
        }
      });

  }

  commentsList(val) {
    $(".listTap").removeClass("active");
    $(".commentDesc").removeClass("active");
    $("#" + val + "com").addClass("active");
    $("#" + val + "coms").addClass("active");
    $("#" + val + val + "com").addClass("active");
    $("#" + val + val + val + "com").addClass("active");
    if (val == 1) {
      this.listComments = false;
    } else {
      this.listComments = true;
    }
  }

  allCommentsList() {
    if (this.token) {
      let list = {
        token: this.token,
        postId: this.itemList.postId
      }
      this._service.allCommentPost(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.allComment = true;
            this.itemList.commentData = res.result;
            this.commentTimeFunction();
            this.hashtagFunction();
          }
        });

    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }


  postCommentSend(type, id) {
    if (this.token) {
      let list = {
        token: this.token,
        postId: id,
        type: type.toString(),
        comment: this.textAreaMsg
      }
      // this.commentMsg.push(list);
      this.textAreaMsg = "";
      this.lengthTextarea = false;
      this._service.commentPost(list)
        .subscribe((res) => {
          this.commentMsg = res.data;
          this.itemList.totalComments += 1;
          this.commentMsg[0].commentData[0].commentedOn = "0m";
          this.itemList.commentData.push(this.commentMsg[0].commentData[0]);
          this.hashtagFunction();
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }

  hashtagFunction() {
    if (this.itemList.commentData) {
      var len = this.itemList.commentData.length;
      for (var i = 0; i < len; i++) {
        if (this.itemList.commentData) {
          var commenttext = this.itemList.commentData;
          // commenttext.splice(x, 0);
          if (this.itemList.commentData[i].commentBody) {
            var postCommentNodeArr1 = commenttext[i].commentBody;
            // //console.log(postCommentNodeArr1);
            var hashtag_regexp = /#([a-zA-Z0-9_]+)/g;
            var comment_text = postCommentNodeArr1.replace(
              hashtag_regexp,
              '<a href="p/$1" class="color">#$1</a>'
            );
            var hashtag_regexp1 = /@([a-zA-Z0-9_]+)/g;
            var comment_textt = comment_text.replace(
              hashtag_regexp1,
              '<a href="p/$1" class="color">@$1</a>'
            );
            this.itemList.commentData[i].commentBody = comment_textt;
          }
        }
      }
    }
  }

  deleteComment(item, i, type, id) {
    let list = {
      token: this.token,
      commentedByUser: item.commentedByUser,
      commentBody: item.commentBody,
      commentId: item.commentId,
      type: type.toString(),
      postId: id
    }
    this.itemList.commentData.splice(i, 1);
    this._service.deleteCommentPost(list)
      .subscribe((res) => {

      });
  }

  emailValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.email_Error = false;
        this.registerSave = true;
      } else {
        this.email_Error = true;
        this.registerSave = false;
      }
    } else {
      this.email_Error = false;
    }
  }

  mobileValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
      if (value.match(regexPhone)) {
        this.phone_Error = false;
        this.registerSave = true;
      } else {
        var val = value.replace(/([^+0-9]+)/gi, '')
        this.sellPhone = val;
        this.phone_Error = true;
        this.registerSave = false;
      }
      if (value.length == 10) {
        this.phone_Error = true;
        this.registerSave = false;
      }
    } else {
      this.phone_Error = false;
    }
  }

  sellHowSend() {
    console.log(this.sellEmail, this.sellPhone);
    if (this.sellEmail && this.sellPhone) {
      this.howPopupSuccess = false;
      this.phone_Error = false;
      this.email_Error = false;
      this.PEerror = "Please try anyone";
      setTimeout(() => {
        this.PEerror = false;
      }, 3000);
    } else if (this.sellEmail || this.sellPhone) {
      if (this.sellEmail) {
        this.sellSend(1);
      } else {
        this.sellSend(2);
      }
      this.PEerror = false;
    }
  }

  emptySell() {
    this.sellPhone = "";
    this.sellEmail = "";
    this.email_Error = false;
    this.phone_Error = false;
  }

  sellSend(val) {
    this.email_Error = false;
    this.phone_Error = false;
    if (val == 1) {
      this.listSell = {
        type: "1",
        emailId: this.sellEmail
      }
    } else {
      this.listSell = {
        type: "2",
        phoneNumber: this.sellPhone
      }
    }
    // console.log(this.listSell);
    this.homeService.sellList(this.listSell)
      .subscribe((res) => {
        if (res.code == 200) {
          this.howPopupSuccess = true;
          this.emptySell();
        } else {
          this.PEerror = res.message;
        }
      });
  }
  imgUrl = [];
  selectedIndex: any;
  editPost(list) {
    console.log(list);
    this.imgUrl = [];
    this.selectedIndex = -1;
    this.postEdit = list;
    let imgUrl = [
      { img: this.postEdit.mainUrl, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight, containerWidth: this.postEdit.containerWidth },
      { img: this.postEdit.imageUrl1, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight1, containerWidth: this.postEdit.containerWidth1 },
      { img: this.postEdit.imageUrl2, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight2, containerWidth: this.postEdit.containerWidth2 },
      { img: this.postEdit.imageUrl3, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight3, containerWidth: this.postEdit.containerWidth3 },
      { img: this.postEdit.imageUrl4, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight4, containerWidth: this.postEdit.containerWidth4 }
    ]
    for (var i = 0; i < imgUrl.length; i++) {
      if (imgUrl[i].img) {
        this.imgUrl.push(imgUrl[i]);
      }
    }
    if (list.isSwap == 1) {
      this.willing = 1;
      this.swapArr = list.swapPost;
      // console.log( this.swapArr)
    }
    this.selectedIndex = this.catList.findIndex((item) => item.categoryNodeId === this.postEdit.categoryNodeId);
    console.log("the seelcted category index:",this.selectedIndex )
    this.selectCat(this.selectedIndex);
    $(".sellModals").modal("show");
  }

  listGetCategories() {
    this.homeService.getCategoriesList(this.languageCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
        }
      });
  }

  selectCurrency(val) {
    this.postEdit.currency = val;
  }

  selectCondition(val) {
    this.postEdit.condition = val;
  }

  subCat: any;
  catSubFilter: any;
  subCategory: any;
  category: any;

  // selectCat(val) {
  //   this.category = this.catList[val].name;
  //   this.categoryId = this.catList[val].categoryNodeId;
  //   this.subCat = "";
  //   this.catSubFilter = "";
  //   this.homeService.getSubCategoriesList(this.category,this.languageCode,this.categoryId)
  //     .subscribe((res) => {
  //       this.subCat = res.data;
  //     })
  // }
  subcaegoryId:any;
  subSelectIndex:any;
  selectCat(val) {
    console.log("the post filter is:", this.postEdit.postFilter);
    let subCatFiler = this.postEdit.postFilter;
    this.category = this.catList[val].name;
    this.categoryId = this.catList[val].categoryNodeId;
    let value ='';
    this.subCat = "";
    this.catSubFilter = "";
    if(this.postEdit.subCategory && this.postEdit.subCategory.length > 0){
      this.homeService.getSubCategoriesList(this.category,this.languageCode,this.categoryId)
      .subscribe((res) => {
        this.subCat = res.data;
        console.log("the subcategory list s:", res.data)
        if(this.postEdit.subCategoryNodeId){
          this.subCat.forEach((item,index) =>{
            if(item.subCategoryNodeId === this.postEdit.subCategoryNodeId){
              console.log("the selected subcategory is:",index,this.postEdit.subCategory );
              this.subCategory = this.postEdit.subCategory;
              this.subSelectIndex = index;
            }
          });
          this.selectSubCat(this.subSelectIndex)
        }
        else{
          this.subSelectIndex = -1;
        }
        // this.subCat.forEach((item,index) =>{
        //   if(item.subCategoryName === this.postEdit.subCategory){
        //     console.log("the selected subcategory is:",index,this.postEdit.subCategory );
        //     this.subCategory = this.postEdit.subCategory;
        //     this.subSelectIndex = index;
        //   }
        // });
        // this.selectSubCat(this.subSelectIndex)
      },error =>{
        console.log("the error is:", error)
      })
    }
    else{
      this.homeService.getSubCategoriesList(this.category,this.languageCode,this.categoryId)
      .subscribe((res) => {
        this.subCat = res.data;
      });
    }
  }
  selectSubCat(i) {
    this.subCategory = this.subCat[i].subCategoryName;
    this.subcaegoryId = this.subCat[i].subCategoryNodeId;
    var value = [];
    this.catSubFilter =[];
    let filteredData = [];
    this.subCat[i].filter.forEach(x => {
      if (x.type == 2 || x.type == 4 || x.type == 6) {
        let values = x.values.split(",");
        values.forEach((item)=>{
          filteredData.push({type:x.type, value:item, checked:false});
        });
        x.filterData = filteredData;
        this.postEdit.postFilter.forEach((item1,index) =>{
          filteredData.forEach((item,index)=>{
               if(item.value === item1.values){
                filteredData[index].checked = true;
               }
          })
        });
        filteredData =[];
        value.push(x);
      }
    });

    this.catSubFilter = value;
    console.log("value", this.catSubFilter)
  }
  // selectSubCat(i) {
  //   this.subCategory = this.subCat[i].subCategoryName;
  //   this.subcaegoryId = this.subCat[i].subCategoryNodeId;
  //   var value = [];
  //   this.subCat[i].filter.forEach(x => {
  //     if (x.type == 2 || x.type == 4 || x.type == 6) {
  //       x.filterData = x.values.split(",");
  //       // x.fieldName = x.id;
  //       // x.value = split;
  //     }
  //     value.push(x);
  //   });

  //   this.catSubFilter = value;
  //   // console.log("value", this.catSubFilter)
  // }

  documentTextList(event, i) {
    // console.log("d", event.target.value.length)
    if (event.target.value.length > 0) {
      if (this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5) {
        this.catSubFilter[i].data = Number(event.target.value);
      } else {
        this.catSubFilter[i].data = event.target.value;
      }
      this.catSubFilter[i].checked = true;
    } else {
      this.catSubFilter[i].checked = false;
    }
  }

  switchToggle(val) {
    if (val == true) {
      this.postEdit.negotiable = 1;
    } else {
      this.postEdit.negotiable = 0;
    }
  }

  willingToggle(val) {
    this.swapArr = [];
    if (val == true) {
      this.willing = 1;
    } else {
      this.willing = 0;
    }
  }

  swapList: any;
  swapSearch(val) {
    let list = {
      token: this.token,
      productName: val
    }
    if (val.length > 0) {
      this.homeService.searchSwap(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.swapList = res.data;
          } else {
            this.swapList = [];
          }
        })
    } else {
      this.swapList = [];
    }
  }

  swapArr = [];
  swapPost: string;
  postSwap(list) {
    let data = {
      swapDescription: list.description,
      swapPostId: list.postId,
      swapTitle: list.productName
    }
    this.swapArr.push(data);
  }

  cancelSwapList(i) {
    this.swapArr.splice(i, 1)
  }

  blurSwap() {
    setTimeout(() => {
      this.swapPost = "";
      this.swapList = [];
    }, 1000)
  }

  cancelSwap() {
    this.swapPost = "";
    this.swapArr = [];
    this.swapList = [];
  }

  submitData(url) {
    // console.log("uuuuuuuu", url);
    if (this.imgUrl && this.imgUrl.length < 5) {
      this.imgUrl.push(url);
      console.log("iiiiiii", this.imgUrl);
    }
  }

  removeImg(i) {
    this.imgUrl.splice(i, 1);
    console.log("rm", this.imgUrl);
  }


  fileUploader() {
    this.uploader.uploadAll();
  }


  updatePost(list) {
    var filData = {};
    if (this.catSubFilter) {
      this.catSubFilter.forEach(x => {
        if (x.checked == true) {
          filData[x.id] = x.data;
        }
      })
    }

    let city = $("#cityP").val();
    let state = $("#stateP").val();
    let location = $("#locateP").val()
    let lat = $("#latP").val();
    let lang = $("#lngP").val();
    if (this.imgUrl.length > 0 && location) {

      let lists = {
        token: this.token,
        type: "0",
        mainUrl: this.imgUrl[0].img,
        thumbnailImageUrl: this.imgUrl[0].img,
        imageCount: this.imgUrl.length,
        containerHeight: this.imgUrl[0].containerHeight,
        containerWidth: this.imgUrl[0].containerWidth,
        cloudinaryPublicId: this.imgUrl[0].cloudinaryPublicId,
        postId: list.postId,
        // price: list.price,
        currency: list.currency,
        productName: list.productName,
        description: list.description,
        condition: "0",
        negotiable: list.negotiable,
        category: this.category || list.category,
        categoryNodeId:this.categoryId,
        // subCategory: this.subCategory,
        isSwap: this.willing,
        swapingPost: JSON.stringify(this.swapArr),
        filter: JSON.stringify(filData),
        location: location,
        latitude: lat,
        longitude: lang,
        city: city,
        countrySname: state,
        imageUrl1: "",
        cloudinaryPublicId1: "",
        imageUrl2: "",
        cloudinaryPublicId2: "",
        imageUrl3: "",
        cloudinaryPublicId3: "",
        imageUrl4: "",
        cloudinaryPublicId4: ""
      }
      if (this.imgUrl && this.imgUrl[1]) {
        lists.imageUrl1 = this.imgUrl[1].img;
        lists.cloudinaryPublicId1 = this.imgUrl[1].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[2]) {
        lists.imageUrl2 = this.imgUrl[2].img;
        lists.cloudinaryPublicId2 = this.imgUrl[2].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[3]) {
        lists.imageUrl3 = this.imgUrl[3].img;
        lists.cloudinaryPublicId3 = this.imgUrl[3].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[4]) {
        lists.imageUrl4 = this.imgUrl[4].img;
        lists.cloudinaryPublicId4 = this.imgUrl[4].cloudinaryPublicId;
      }
      // let price = this.editedEurCurrency.replace('.','').replace(',','.');
      // let price = this.editedEurCurrency.split('.').join('').split(',').join('.');
      // (this.item.price.toString() === price.toString()) ? 
      // list.price = this.item.price : list.price = parseFloat(this.editedEurCurrency.replace(',','.'));
      let price;
      console.log("the price is:, ",this.editedEurCurrency);
      this.editedEurCurrency.lastIndexOf(',') <= -1  ?
      price = this.editedEurCurrency.split('.').join('') : price = this.editedEurCurrency.split('.').join('');
      console.log("the modified price is:",price );
      lists['price'] = parseFloat(price.replace(",","."));
      // this.editedEurCurrency.lastIndexOf(',') <= -1 ?
      // price = this.editedEurCurrency.split('.').join('') : price = this.editedEurCurrency.split('.').join('').split(',').join('.');
      // if(this.editedEurCurrency.lastIndexOf(',') > -1){
      //   price = (this.editedEurCurrency.split(',').join('.'));
      //   (this.item.price.toString() === price.toString()) ? 
      //   list.price = this.item.price : list.price = parseFloat(this.editedEurCurrency.replace(',','.'));
      // }

      // list.price = parseFloat(priceis);
      // this.subCategory && this.subCat.length > 0 ? lists['subCategory']= this.subCategory : lists['subCategory'] = "0";
      // this.subCategory ? lists['subCategory'] = this.subCategory : lists['subCategory'] = "0";
      if(this.subCat && this.subCat.length > 0 && this.subCategory){
      
        list['subCategory']= this.subCategory;
        list['subCategoryNodeId'] = this.subcaegoryId;
        console.log("the list for update post is:", lists)
        this.EditPost(lists);
      //   this.loaderButton = true;
      // setTimeout(() => {
      //   this.loaderButton = false;
      //   this.errMsg = false;
      // }, 3000);
      // this.homeService.postEditProduct(lists)
      //   .subscribe((res) => {
      //     this.loaderButton = false;
      //     if (res.code == 200) {
      //       this._router.navigate(["./settings"]);
      //       $(".modal").modal("hide");
      //     }
      //   }, err => {
      //     this.loaderButton = false;
      //     console.log(err._body)
      //     var error = JSON.parse(err._body);
      //     this.errMsg = error.message;
      //   });
      }
      else{
        
        if(typeof this.subCat == "undefined"){
          list['subCategory'] = "0";
          this.EditPost(lists);
          console.log("the list for update post is:", lists)
        }
        else {
          this.errMsg = "Mandatory field is missing";
        }
      }
    }
  }

  EditPost(lists){
    this.loaderButton = true;
          setTimeout(() => {
            this.loaderButton = false;
            this.errMsg = false;
          }, 3000);
          this.homeService.postEditProduct(lists)
            .subscribe((res) => {
              this.loaderButton = false;
              if (res.code == 200) {
                this._router.navigate(["./settings"]);
                $(".modal").modal("hide");
                this.valueToshow = '';
                this.valueArr =[];
              }
            }, err => {
              this.loaderButton = false;
              console.log(err._body)
              var error = JSON.parse(err._body);
              this.errMsg = error.message;
            });
  }

  delPostId: any;
  deletePosts(list) {
    $("#deletePost").modal("show");
    this.delPostId = list.postId;
  }

  deletePost() {
    this.homeService.postDeleteProduct(this.token, this.delPostId)
      .subscribe((res) => {
        if (res.code == 200) {
          $("#deletePost").modal("hide");
          this._router.navigate(["./settings"]);
        }
      })
  }


  changeLocation() {
    var input = document.getElementById('sellLocates');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === 'country') {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
        }
      }
      // console.log("cityArea", place);
      if (City) {
        $("#cityP").val(City);
      } else if (state) {
        $("#cityP").val(state);
      } else {
        $("#cityP").val(place.formatted_address);
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      $("#latP").val(lat);
      $("#lngP").val(lng);
      $("#stateP").val(state);
      $("#locateP").val(place.formatted_address);
    });
  }




  onFacebookLoginClick(): void {
    FB.ui({
      method: 'share',
      mobile_iframe: true,
      href: 'https://developers.facebook.com/docs/',
    }, function (response) {
      console.log(response);
    });
  }


  locationMap() {
    var myLatLng = { lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) };
    var mapCanvas = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 15,
      center: myLatLng,
      scrollwheel: false
    });

    setTimeout(() => {
      google.maps.event.trigger(mapCanvas, "resize");
      mapCanvas.setCenter({ lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) });
    }, 300);
    var markerIcon = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: 'rgba(167, 217, 206, 0.81)',
      fillOpacity: .9,
      scale: 25,
      strokeColor: 'rgba(167, 217, 206, 0.81)',
      strokeWeight: 1
    };
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: mapCanvas,
      icon: markerIcon
    });
  }

  popupLocation() {
    var myLatLng = { lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) };

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: myLatLng,
      scrollwheel: false
    });
    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter({ lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) });
    }, 300);
    var markerIcon = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: 'rgba(167, 217, 206, 0.81)',
      fillOpacity: .9,
      scale: 50,
      strokeColor: 'rgba(167, 217, 206, 0.81)',
      strokeWeight: 1
    };
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: markerIcon
    });

  }

  filterToggle() {
    // $(".filterRecent").addClass("hide");
    // $(".filterRecentH").addClass("hide");
    // $(".filterRecentF").addClass("hide");
    $("#sideDetailsId").toggleClass('active');
  }
  // function : copyUrl, developer: sowmya sv , desc: to show sharing the current url
  copyUrl(caseVal){
    switch(caseVal){
      case 0: this.ShowCopyUrl = true;
      console.log('the --------------------------------')
              this.currentUrlLocation = window.location.href;
              break;

      case 1: this.ShowCopyUrl = false;
              let  copyText = document.getElementById("shareUrl") as HTMLInputElement;
              copyText.select();
              let successfulCopy =  document.execCommand("copy");
              break;
    }
  }
  addCommaToPrice(val){
   var str = val.toString().split('.');
   if (str[0].length >= 4) {
      //  str[0] = str[0].match(/.{1,3}/g).join('.');
      str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
   }
   if (str[1] && str[1].length >= 2) {
    str[1] = str[1].substr(0,2);  
      //  str[1] = str[1].replace(/(\d{2})/g, '$1 ');
   }
   return str.join(',');
 }
 addOnlyNumComma(event){
  // const pattern = /^[0-9,]*$/;  
  const pattern = /^[0-9]*[,]{0,1}[0-9]{0,2}$/;
  this.valueToshow = event.target.value;
  if(!pattern.test(event.target.value)){
    event.target.value = event.target.value.replace(/[^0-9,]/g, '').replace(/(\..*)\./g, '$1');
    if(this.valueToshow.lastIndexOf(',') > -1){
      this.valueArr = this.valueToshow.split(',');
      console.log("the valuetoShow is:", this.valueArr);
      (this.valueArr.length == 1)? this.valueToshow = this.valueArr[0]: this.valueToshow = this.valueArr[0]+','+ this.valueArr[1].slice(0,2);
      event.target.value = event.target.value.replace(event.target.value, this.valueToshow)
    }
    console.log("the mismatched");
  }
}
}
  
