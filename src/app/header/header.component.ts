import { Component, OnInit, HostListener, AfterViewInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/platform-browser';
import { MissionService } from '../app.service';
import { HomeService } from '../home/home.service';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angularx-social-login";
import { TranslateService} from '@ngx-translate/core';


declare var $: any;
declare var google: any;
declare var FB: any;
declare var gapi: any;

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  searchLayer: boolean = true;
  signUpContent: boolean = true;
  loginSignUpFB: boolean = false;
  categoryNodeId:any;
  subCategoryNodeId:any;
  header: any;
  closeShow = false;
  sellToggle = false;
  PhoneNum:any;
  productPrice1 = new FormControl('',[Validators.pattern('[a-zA-Z ]*')])
  languageOptions: any = [{"lang":"Serbian","checked":false,"value":1},
  {"lang":"Montenegro","checked":false,"value":2},
  {"lang":"English","checked":false,"value":3}];
  constructor(
    private missionService: MissionService,
    private _conf: Configuration,
    private _zone: NgZone,
    private _router: Router,
    private homeService: HomeService,
    private _lang: LanguageService,
    public _auth: AuthService,
    public translate: TranslateService
  ) {
    missionService.missionheaderOpen$.subscribe(
      headerOpen => {
        // this.headerListOpen = true;
        // this.headerListHelpOpen = false;
        // $(".headerMenu").addClass("headerOpenactive");
        // $(".HeaderPageList").addClass("container");
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = false;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });
    missionService.missionheaderClose$.subscribe(
      headerClose => {
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = true;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });
    missionService.missionheaderClosed$.subscribe(
      headerClosed => {
        // this.headerListOpen = false;
        // this.headerListHelpOpen = false;
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = false;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });
    missionService.missionheaderHelpClose$.subscribe(
      headerHelpClose => {
        // this.headerListHelpOpen = true;
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = false;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });

    missionService.missionServer$.subscribe(
      serverError => {
        $("#serverError").modal("show");
      });

    missionService.missionPopup$.subscribe(
      headerRefresh => {
        this.loginSignUp();
      });

    missionService.missionheaderRefresh$.subscribe(
      headerRefresh => {
        this.ngOnInit();
      });
  }
  IsSelling:any;
  headerFilter = false;
  headerOpen = false;
  menuListDrop = false;
  headerListOpen = false;
  headerListHelpOpen = false;
  radioSelectList = false;
  loginList = false;
  token: any;
  userName: any;
  profilePicUrl: any;
  searchCat: any;
  headerRefresh: string;
  phoneNumber: string;
  email: string;
  otpNumber: string;
  deviceOs: string;
  registerSave = false;
  phone_Error = false;
  email_Error: any;
  succesSeller = false;
  placelatlng: any;
  listSell: any;
  sideListMenu = false;
  login_Toggle = false;
  signUp_toggle = false;
  SocialToggle = false;
  forgot_Toggle = false;
  otp_Toggle = false;
  registerPhoneErrMsg: any;
  registerUserErrMsg: any;

  username: string;
  phonenumber: string;
  password: string;
  registerErrMsg: any;
  loaderButton = false;
  loaderButtonfb = false;
  fullName: string;
  registerEmailErrMsg: any;
  loginModal = false;
  registerfullErrMsg: any;
  sub: any;
  accessToken: any;

  catList: any;
  toggleSlide = false;
  willingSlide = false;

  productName: string;
  productDescription: string;
  productPrice: string;
  currency: string;
  condition: string;
  category: string;
  subCategory: string;
  negotiable = "0";
  willing = 0;
  subCat: any;
  catSubFilter: any;
  imgUrl = [];
  errMsg: any;
  cloudinaryImages: any;
  singnImg: any;
  languageCode:string = 'rs';
  cloudinaryOptions: CloudinaryOptions = new CloudinaryOptions({
    cloudName: 'yeloadmin',
    uploadPreset: 'iirvrtog',
    autoUpload: true
  });

  uploader: CloudinaryUploader = new CloudinaryUploader(this.cloudinaryOptions);
  uploaders: CloudinaryUploader = new CloudinaryUploader(this.cloudinaryOptions);

  imageUploader = (item: any, response: string, status: number, headers: any) => {
    let cloudinaryImage = JSON.parse(response);
    console.log(cloudinaryImage)
    let list = {
      img: cloudinaryImage.secure_url,
      cloudinaryPublicId: cloudinaryImage.public_id,
      containerHeight: cloudinaryImage.height,
      containerWidth: cloudinaryImage.width
    }
    this.submitData(list);
  };

  imageUploaders = (item: any, response: string, status: number, headers: any) => {
    let cloudinaryImage = JSON.parse(response);
    console.log(cloudinaryImage);
    this.submitSignup(cloudinaryImage.secure_url);
  };

  ngOnInit() {
    this.uploader.onSuccessItem = this.imageUploader;
    this.uploaders.onSuccessItem = this.imageUploaders;
    // this._conf.setItem("Language",1);
    // let selectedLang  = Number(this._conf.getItem("Language"));
    let selectedLang  = Number(sessionStorage.getItem("Language"));
    if(selectedLang){
      switch(selectedLang){
        case 1:this.header = this._lang.engHeader1;
               this.languageCode = 'sr';
               break;

        // this.placelatlng = {
        //   'lat':'44.0165',
        //   'lng':'21.0059'
        // }
        // this._conf.setItem('latlng',JSON.parse(this.placelatlng));
        case 2:this.header = this._lang.engHeader1;
               this.languageCode = 'srp';
               break;
        // this.placelatlng = {
        //   'lat':'42.7087',
        //   'lng':'19.3744'
        // }
        // this._conf.setItem('latlng',JSON.parse(this.placelatlng));

        case 3: this.header = this._lang.engHeader;
                this.languageCode = 'en';
                break;
      }
    }
    else{
      this.header = this._lang.engHeader1;
      this.languageCode = 'sr';
      console.log('the language options :',this.header.languageOptions);
      // this.placelatlng = {
      //   'lat':'44.0165',
      //   'lng':'21.0059'
      // }
      // this._conf.setItem('latlng',JSON.parse(this.placelatlng));
    }
    // get to know which langauge is selected
    this.header.languageOptions.forEach((element,index) => {
      if(element.langCode === this.languageCode){
       this.header.languageOptions[index].checked = true;
      }
      else{
       this.header.languageOptions[index].checked = false;
       this.header.languageOptions[index].checked = false;
      }
    });
    FB.init({
      appId: '895123320630756',
      cookie: false,
      xfbml: true,
      version: 'v2.7'
    });
    let latlng1 = this._conf.getItem('latlng');
    if (!latlng1) {
      setTimeout(()=>{
        this.currentLocation();
      },100)
      // this.currentLocation();
    }
    this.token = this._conf.getItem('authToken');
    this.profilePicUrl = this._conf.getItem('profilePicUrl');
    if (this.token) {
      console.log("true");
      this.userName = this._conf.getItem('username');
      this.loginList = true;
      // $(".headerMenu").removeClass("loginDetailsHeader");
      this.userReject();
    } else {
      console.log("false");
      this.loginList = false;
      // $(".headerMenu").addClass("loginDetailsHeader");
    }
    $('ul.dropdown-menu').on('click', function (event) {
      event.stopPropagation();
    });

    this.listGetCategories();
    $.getJSON("https://restcountries.eu/rest/v1/alpha/in", (data) => {
      // console.log(data.currencies)
      // this.currency = data.currencies[0];
      this.currency = "EUR";
    });

    $("#signPwd").keydown((event) => {
      if (event.keyCode == 32) {
        event.preventDefault();
      }
    });

  }

  listGetCategories() {
    this.homeService.getCategoriesList(this.languageCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
        }
      });
  }

  homeLogo() {
    $("html, body").animate({ scrollTop: 0 }, 500);
    this._router.navigate([""]);
  }

  currentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        if(lat.toFixed(4).toString() == '42.0931')
        {
           lat = 42.4411;
        }
        if(lng.toFixed(4).toString() == '19.2636'){
          lng = 19.2636;
        }
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
        console.log("currentLatLng", this.placelatlng,position.coords);
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (var i = 0; i < results[1].address_components.length; i += 1) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === 'locality') {
                    // console.log(addressObj.types[j]);
                    var city = addressObj.long_name;
                    // console.log(results[0].formatted_address)                   
                  }
                  if (addressObj.types[j] === 'country') {
                    // console.log(addressObj.types[j]);
                    var country = addressObj.short_name;
                    // console.log(results[0].formatted_address)                   
                  }
                }
              }
              // console.log("cityCountry", city + ", " + country)
              $("#cityAreai").val(city + ", " + country);
              this._conf.setItem('recentCity', city + ", " + country);
              $("#searchLocation1").val(city + ", " + country);
            }
            // console.log(results)
          }
        });

      }, this.geoError);
    } else {
      console.log("Geolocation is not supported by this browser.");
    }
  }

  geoError() {
    console.log("Geolocation is not supported by this browser.");
  }


  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = $(window).scrollTop();
    // if (number > 20) {
    //   this.headerOpen = true;
    // } else {
    //   this.headerOpen = false;
    // }
  }

  userReject() {
    let list = {
      latitude: "",
      longitude: "",
      token: this.token,
      offset: "0",
      limit: "40"
    }
    this.placelatlng = this._conf.getItem('latlng');
    if (this.placelatlng) {
      this.placelatlng = JSON.parse(this.placelatlng);
      list.latitude = this.placelatlng.lat;
      list.longitude = this.placelatlng.lng;
    }
    this.homeService.homeAllList(list)
      .subscribe((res) => {
        if (res.code == 200) {

        }
      }, (err) => {
        var error = JSON.parse(err._body);
        var listErr = error.code;
        if (listErr == 401) {
          // console.log(error);
          this._conf.clear();
          $("#userRejected").modal("show");
        }
      });
  }




  radioSelect(val) {
    this.emptySell();
    if (val != 1) {
      this.radioSelectList = true;
    } else {
      this.radioSelectList = false;
    }
  }
  succesSellerList() {
    // $.get('https://ipapi.co/json', function () { }).always(function (resp) {
    //   this._conf.setItem('country',resp.country)
    // });
    setTimeout(()=>{
      $("#phoneSell").intlTelInput({
      nationalMode: true,
      separateDialCode: true,
      initialCountry: this._conf.getItem('country'),

      autoPlaceholder: false,
      // modify the auto placeholder
      customPlaceholder: null,
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
    $("#sellModal").modal("show");
    this.succesSeller = false;
    this.emptySell();
  },100);
    // this._router.navigate(["./sell"]);
  }


  emptySell() {
    this.phoneNumber = "";
    this.email = "";
    this.email_Error = false;
    this.phone_Error = false;
  }

  sellSend(val) {
    this.email_Error = false;
    this.phone_Error = false;
    if (val == 1) {
      this.listSell = {
        type: "1",
        emailId: this.email
      }
    } else {
      let flag = $("#phoneSell").intlTelInput("getSelectedCountryData");
      this.listSell = {
        type: "2",
        phoneNumber: "+" + flag.dialCode + this.phoneNumber
      }
    }
    this.homeService.sellList(this.listSell)
      .subscribe((res) => {
        if (res.code == 200) {
          this.succesSeller = true;
          this.radioSelectList = false;
          this.emptySell();
        } else {
          this.email_Error = res.message;
        }
      });
  }


  searchCategory(val) {
    this._router.navigate([""]);
    console.log("the category name is :",this.searchCat,val);
    if (val == 1 ) {
      this.closeShow = true;
    } else {
      this.closeShow = true;
      // if(this.searchCat.length > 0)
      this.missionService.confirmcatName(this.searchCat);
    }
  }

  cancelSearch() {
    this._router.navigate([""]);
    this.closeShow = false;
    this.missionService.confirmcatName("1");
    this.searchCat = "";
  }

  menuListDropdown1() {
    $(".listMenu").removeClass("open");
    $(".firstMenu").addClass("open");
    $(".firstMenu").removeClass("opened");
  }
  menuListDropdown() {
    $(".firstMenu").addClass("opened");
    $(".listMenu").removeClass("open");
    $(".secondMenu").addClass("open");
  }

  logOut() {

    let google = this._conf.getItem("google");
    if (google == '1') {
      let url = window.location.href;
      document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=" + url;
    }
    this._conf.clear();
    this.missionService.confirmheaderRefresh(this.headerRefresh);
    this._router.navigate([""]);
    window.location.replace('');   

    // var auth2 = gapi.auth2.getAuthInstance();
    // auth2.signOut().then(function () {
    //   console.log('User signed out.');
    // });
  }

  logOutReject() {
    this._conf.clear();
    window.location.replace('');
  }


  // onFacebookLoginClick(): void {
  //   FB.ui({
  //     method: 'send',
  //     link: 'http://www.nytimes.com/interactive/2015/04/15/travel/europe-favorite-streets.html',
  //   });
  // }


  serverError() {
    window.location.replace('');
  }

  searchLayerToggle() {
    this.searchLayer = false;
  }

  removeLayer() {
    this.searchLayer = true;
    $("#search_input").val("");
  }

  filterToggle() {
    // $(".filterRecent").addClass("hide");
    // $(".filterRecentH").addClass("hide");
    // $(".filterRecentF").addClass("hide");
    $("#sideDetailsId").animate({ scrollTop: 0 }, 500);
    $("#sideDetailsId").toggleClass('active');
    $("body").toggleClass("hideHidden");
  }

  sideMenuOpen() {
    $("html, body").animate({ scrollTop: 0 }, 500);
    this.sideListMenu = !this.sideListMenu;
  }


  loginSignUp() {
    // $.get('https://ipapi.co/json', function () { }).always(function (resp) {
    //   console.log("the  response from ipapi is:", resp,resp.country);
    //   console.log(JSON.stringify(resp, null, 2));
    //   this._conf.setItem('country',resp.country)
    // });
    // $.getJSON('https://ipapi.co/json/', function(data) {
    //   console.log(JSON.stringify(data, null, 2));
    //   console.log("the country is here;",this._conf.setItem('country',data.country));
    // });
    setTimeout(()=>{
      $("#phone").intlTelInput({
      nationalMode: true,
      separateDialCode: true,
      initialCountry: this._conf.getItem('country'),

      autoPlaceholder: false,
      // modify the auto placeholder
      customPlaceholder: null,
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
    $("#loginModal").modal("show");
    $("#loginModal").modal("show");
    this.socialLink();
  },3000);
  // this.CountDownTimer();

    // $("#phone").intlTelInput({
    //   nationalMode: true,
    //   separateDialCode: true,
    //   initialCountry: "auto",
    //   geoIpLookup: function (callback) {
    //     $.get('//ipapi.co/json', function () { }, "jsonp").always(function (resp) {
    //       var countryCode = (resp && resp.country) ? resp.country : "";
    //       callback(countryCode);
    //     });
    //   },
    //   autoPlaceholder: false,
    //   // modify the auto placeholder
    //   customPlaceholder: null,
    //   utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    // });
    // $("#loginModal").modal("show");
    // this.socialLink();
  }
  minuteS:any = '3';
  Seconds:any ='00';
  interval:any;
  CountDownTimer(){
    let minute:any;
    let second:any;
    let timer :any;
    let duration = 3*60;
    timer = duration;
      this.interval = setInterval(()=>{
        minute = parseInt((timer / 60).toString(), 10)
        second = parseInt((timer % 60).toString(), 10);
        this.minuteS = minute;
        this.Seconds = second;
        this.minuteS = this.minuteS < 10 ? "0" + this.minuteS : this.minuteS;
        this.Seconds = this.Seconds < 10 ? "0" + this.Seconds : this.Seconds;
        if (--timer < 0) {
          clearInterval(this.interval)
      }
      },1000) 
  }
  loginToggle() {
    this.login_Toggle = true;
    this.signUp_toggle = false;
    this.SocialToggle = true;
    this.otp_Toggle = false;
    this.forgot_Toggle = false;
    this.registerValue();
  }
  signUpToggle() {
    this.login_Toggle = false;
    this.signUp_toggle = true;
    this.SocialToggle = true;
    this.forgot_Toggle = false;
  }
  socialLink() {
    this.registerValue();
    this.login_Toggle = false;
    this.signUp_toggle = false;
    this.SocialToggle = false;
    this.otp_Toggle = false;
    this.forgot_Toggle = false;
  }

  resetToggle() {
    this.registerValue();
    this.login_Toggle = false;
    this.signUp_toggle = false;
    this.forgot_Toggle = true;
  }

  otpToggle() {
    this.SocialToggle = true;
    this.loaderButton = false;
    this.login_Toggle = false;
    this.signUp_toggle = false;
    this.forgot_Toggle = false;
    this.otp_Toggle = true;
  }

  sellPopup() {
    if (this.token) {
      this.restSell();
      this.imgUrl = [];
      $("#sellModals").modal("show");
    } else {
      this.loginSignUp();
    }
  }

  restSell() {
    this.productPrice = "";
    this.productName = "";
    this.productDescription = "";
    $("#selCat").val('');
    $("#selProd").val('');
    // this.condition = "";
    // this.category = "";
  }

  selectCurrency(val) {
    this.currency = val;
  }

  selectCondition(val) {
    this.condition = val;
  }

  selectCat(val) {
    console.log('the category list is :', val,this.catList[val])
    this.category = this.catList[val].name;
    this.categoryNodeId = this.catList[val].categoryNodeId;
    this.subCat = "";
    this.catSubFilter = "";
    this.homeService.getSubCategoriesList(this.category,this.languageCode,this.categoryNodeId)
      .subscribe((res) => {
        this.subCat = res.data;
      })
  }

  selectSubCat(i) {
    this.subCategory = this.subCat[i].subCategoryName;
    this.subCategoryNodeId = this.subCat[i].subCategoryNodeId;
    console.log("the subcategory is :",this.subCategory,this.subCat[i].subCategoryName, i)
    var value = [];
    this.subCat[i].filter.forEach(x => {
      if (x.type == 2 || x.type == 4 || x.type == 6) {
        x.filterData = x.values.split(",");
        // x.fieldName = x.id;
        // x.value = split;
      }
      value.push(x);
      console.log("filter value is :", value)
    });
    if(value.length > 0){
      this.catSubFilter = value;
    }
    // this.catSubFilter = value;
    console.log("value", this.catSubFilter)
  }

  documentTextList(event, i) {
    // console.log("d", event.target.value.length)
    if (event.target.value.length > 0) {
      if (this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5) {
        this.catSubFilter[i].data = Number(event.target.value);
      } else {
        this.catSubFilter[i].data = event.target.value;
      }
      this.catSubFilter[i].checked = true;
    } else {
      this.catSubFilter[i].checked = false;
    }
  }

  switchToggle(val) {
    if (val == true) {
      this.negotiable = "1";
    } else {
      this.negotiable = "0";
    }
  }

  willingToggle(val) {
    this.swapArr = [];
    if (val == true) {
      this.willing = 1;
    } else {
      this.willing = 0;
    }
  }

  uploadProduct() {
    this.condition = "0";
    var filData = {};
    if (this.catSubFilter) {
      this.catSubFilter.forEach(x => {
        if (x.checked == true) {
          console.log('the data is:', x.data)
          filData[x.id] = x.data;
        }
      })
    }
    (this.productPrice.lastIndexOf(',') > -1)? this.productPrice = this.productPrice.replace(',','.'):
    this.productPrice = this.productPrice;
    let city = $("#cityS").val();
    let state = $("#state").val();
    let location = $("#locate").val()
    let lat = $("#latS").val();
    let lang = $("#lngS").val();
    setTimeout(() => {
      this.loaderButton = false;
      this.errMsg = false;
    }, 3000);
    console.log("the imgae is ")
    if (this.imgUrl.length > 0 && location && this.category) {
      this.errMsg = false;
      let list = {
        token: this.token,
        type: "0",
        mainUrl: this.imgUrl[0].img,
        thumbnailImageUrl: this.imgUrl[0].img,
        imageCount: this.imgUrl.length,
        containerHeight: this.imgUrl[0].containerHeight,
        containerWidth: this.imgUrl[0].containerWidth,
        cloudinaryPublicId: this.imgUrl[0].cloudinaryPublicId,
        price: this.productPrice,
        currency: this.currency,
        productName: this.productName,
        description: this.productDescription,
        condition: this.condition,
        negotiable: this.negotiable,
        category: this.category,
        categoryNodeId:this.categoryNodeId,
        // subCategory: this.subCategory,
        isSwap: this.willing,
        swapingPost: JSON.stringify(this.swapArr),
        filter: JSON.stringify(filData),
        location: location,
        latitude: lat,
        longitude: lang,
        city: city,
        countrySname: state,
        imageUrl1: "",
        cloudinaryPublicId1: "",
        imageUrl2: "",
        cloudinaryPublicId2: "",
        imageUrl3: "",
        cloudinaryPublicId3: "",
        imageUrl4: "",
        cloudinaryPublicId4: ""
      }
      if (this.imgUrl && this.imgUrl[1]) {
        list.imageUrl1 = this.imgUrl[1].img;
        list.cloudinaryPublicId1 = this.imgUrl[1].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[2]) {
        list.imageUrl2 = this.imgUrl[2].img;
        list.cloudinaryPublicId2 = this.imgUrl[2].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[3]) {
        list.imageUrl3 = this.imgUrl[3].img;
        list.cloudinaryPublicId3 = this.imgUrl[3].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[4]) {
        list.imageUrl4 = this.imgUrl[4].img;
        list.cloudinaryPublicId4 = this.imgUrl[4].cloudinaryPublicId;
      }
      if(this.subCat && this.subCat.length > 0 && this.subCategory){
      
        list['subCategory']= this.subCategory;
        list['subCategoryNodeId'] = this.subCategoryNodeId;
        this.loaderButton = true;
        this.uploadedProduct(list);
      }
      else{
        
        if(typeof this.subCat == "undefined"){
          list['subCategory'] = "";
          this.uploadedProduct(list);
        }
        else {
          this.errMsg = "Mandatory field is missing";
        }
      }
      console.log("the posted details are 99999:", list)
    } else {
      console.log("the mandatory");
      this.errMsg = "Mandatory field is missing";
    }
  }

  uploadedProduct(list){
    this.loaderButton = true;
    this.homeService.postProduct(list)
    .subscribe((res) => {
      this.loaderButton = false;
      if (res.code == 200) {
        this._router.navigate(["./settings"]);
        this.missionService.confirmheaderRefresh(this.headerRefresh);
        $(".modal").modal("hide");
        this.valueToshow = '';
        this.valueArr =[];
      }
      else if(res.code != 204 || res.code != 200){
        this.errMsg = "Mandatory field is missing";
      }
    }, err => {
      this.loaderButton = false;
      var error = JSON.parse(err._body);
      this.errMsg = error.message;
    });
  }

  swapList: any;
  swapSearch(val) {
    let list = {
      token: this.token,
      productName: val
    }
    if (val.length > 0) {
      this.homeService.searchSwap(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.swapList = res.data;
          } else {
            this.swapList = [];
          }
        })
    } else {
      this.swapList = [];
    }
  }

  swapArr = [];
  swapPost: string;
  postSwap(list) {
    let data = {
      swapDescription: list.description,
      swapPostId: list.postId,
      swapTitle: list.productName
    }
    this.swapArr.push(data);
  }

  cancelSwapList(i) {
    this.swapArr.splice(i, 1)
  }

  blurSwap() {
    setTimeout(() => {
      this.swapPost = "";
      this.swapList = [];
    }, 1000)
  }

  cancelSwap() {
    this.swapPost = "";
    this.swapArr = [];
    this.swapList = [];
  }

  submitData(url) {
    // console.log("uuuuuuuu", url);
    if (this.imgUrl && this.imgUrl.length < 5) {
      this.imgUrl.push(url);
      // console.log("iiiiiii", this.imgUrl);
    }
  }

  removeImg(i) {
    this.imgUrl.splice(i, 1);
    // console.log("rm", this.imgUrl);
  }


  fileUploader() {
    this.uploader.uploadAll();
  }

  public currencyList = [
    { value: "EUR", text:"montenegro" },
    { value: "DIN", text:"Serbian dinar"}
    // { value: "USD", text: "United States Dollars" },
    // { value: "EUR", text: "Euro" },
    // { value: "GBP", text: "United Kingdom Pounds" },
    // { value: "DZD", text: "Algeria Dinars" },
    // { value: "ARP", text: "Argentina Pesos" },
    // { value: "AUD", text: "Australia Dollars" },
    // { value: "ATS", text: "Austria Schillings" },
    // { value: "BSD", text: "Bahamas Dollars" },
    // { value: "BBD", text: "Barbados Dollars" },
    // { value: "BEF", text: "Belgium Francs" },
    // { value: "BMD", text: "Bermuda Dollars" },
    // { value: "BRR", text: "Brazil Real" },
    // { value: "BGL", text: "Bulgaria Lev" },
    // { value: "CAD", text: "Canada Dollars" },
    // { value: "CLP", text: "Chile Pesos" },
    // { value: "CNY", text: "China Yuan Renmimbi" },
    // { value: "CYP", text: "Cyprus Pounds" },
    // { value: "CSK", text: "Czech Republic Koruna" },
    // { value: "DKK", text: "Denmark Kroner" },
    // { value: "NLG", text: "Dutch Guilders" },
    // { value: "XCD", text: "Eastern Caribbean Dollars" },
    // { value: "EGP", text: "Egypt Pounds" },
    // { value: "FJD", text: "Fiji Dollars" },
    // { value: "FIM", text: "Finland Markka" },
    // { value: "FRF", text: "France Francs" },
    // { value: "DEM", text: "Germany Deutsche Marks" },
    // { value: "XAU", text: "Gold Ounces" },
    // { value: "GRD", text: "Greece Drachmas" },
    // { value: "HKD", text: "Hong Kong Dollars" },
    // { value: "HUF", text: "Hungary Forint" },
    // { value: "ISK", text: "Iceland Krona" },
    // { value: "INR", text: "India Rupees" },
    // { value: "IDR", text: "Indonesia Rupiah" },
    // { value: "IEP", text: "Ireland Punt" },
    // { value: "ILS", text: "Israel New Shekels" },
    // { value: "ITL", text: "Italy Lira" },
    // { value: "JMD", text: "Jamaica Dollars" },
    // { value: "JPY", text: "Japan Yen" },
    // { value: "JOD", text: "Jordan Dinar" },
    // { value: "KRW", text: "Korea(South) Won" },
    // { value: "LBP", text: "Lebanon Pounds" },
    // { value: "LUF", text: "Luxembourg Francs" },
    // { value: "MYR", text: "Malaysia Ringgit" },
    // { value: "MXP", text: "Mexico Pesos" },
    // { value: "NLG", text: "Netherlands Guilders" },
    // { value: "NZD", text: "New Zealand Dollars" },
    // { value: "NOK", text: "Norway Kroner" },
    // { value: "PKR", text: "Pakistan Rupees" },
    // { value: "XPD", text: "Palladium Ounces" },
    // { value: "PHP", text: "Philippines Pesos" },
    // { value: "XPT", text: "Platinum Ounces" },
    // { value: "PLZ", text: "Poland Zloty" },
    // { value: "PTE", text: "Portugal Escudo" },
    // { value: "ROL", text: "Romania Leu" },
    // { value: "RUR", text: "Russia Rubles" },
    // { value: "SAR", text: "Saudi Arabia Riyal" },
    // { value: "XAG", text: "Silver Ounces" },
    // { value: "SGD", text: "Singapore Dollars" },
    // { value: "SKK", text: "Slovakia Koruna" },
    // { value: "ZAR", text: "South Africa Rand" },
    // { value: "KRW", text: "South Korea Won" },
    // { value: "ESP", text: "Spain Pesetas" },
    // { value: "XDR", text: "Special Drawing Right(IMF)" },
    // { value: "SDD", text: "Sudan Dinar" },
    // { value: "SEK", text: "Sweden Krona" },
    // { value: "CHF", text: "Switzerland Francs" },
    // { value: "TWD", text: "Taiwan Dollars" },
    // { value: "THB", text: "Thailand Baht" },
    // { value: "TTD", text: "Trinidad and Tobago Dollars" },
    // { value: "TRL", text: "Turkey Lira" },
    // { value: "VEB", text: "Venezuela Bolivar" },
    // { value: "ZMK", text: "Zambia Kwacha" },
    // { value: "EUR", text: "Euro" },
    // { value: "XCD", text: "Eastern Caribbean Dollars" },
    // { value: "XDR", text: "Special Drawing Right(IMF)" },
    // { value: "XAG", text: "Silver Ounces" },
    // { value: "XAU", text: "Gold Ounces" },
    // { value: "XPD", text: "Palladium Ounces" },
    // { value: "XPT", text: "Platinum Ounces" },
    // { value: "QAR", text:"Qatari Riyal"}
  ]


  resetPassword() {
    let list = {
      email: this.email,
      type: '0'
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 1000);
    if (this.email) {
      this.homeService.resetPwd(list)
        .subscribe((res) => {
          this.loaderButton = false;
          if (res.code == 200) {
            this.email = "";
            this.registerErrMsg = res.message;
            setTimeout(() => {
              this.loginToggle();
            }, 3000);
          } else {
            this.registerErrMsg = res.message;
          }
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
        });
    } else {
      this.loaderButton = false;
      (this.languageCode == 'sr' || this.languageCode == 'srp') ? this.registerErrMsg = "popunite sva polja":
      this.registerErrMsg = "field is missing";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
  }




  resetValue() {
    this.username = "";
    this.password = "";
  }

  login() {
    let list = {
      username: this.username,
      password: this.password,
      loginType: 1,
      pushToken: "",
      place: "",
      city: "",
      countrySname: "",
      latitude: "",
      longitude: ""
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    if (this.username && this.password) {
      this.homeService.loginYelo(list)
        .subscribe((res) => {
          // this.loaderButton = false;
          if (res.code == 200) {
            this._conf.setItem('authToken', res.token);
            this._conf.setItem('email', res.email);
            this._conf.setItem('username', res.username);
            this._conf.setItem('userId', res.userId);
            this._conf.setItem('profilePicUrl', res.profilePicUrl);
            this._conf.setItem('mqttId', res.mqttId);
            this.registerErrMsg = false;
            this.resetValue();
            this.logDevice();
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            $(".modal").modal("hide");
            this._router.navigate([""]);
            // window.location.replace('');


          } else {
            this.loaderButton = false;
            this.registerErrMsg = res.message;
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 3000);
          }
        });
    } 
    // else if(this.password.length < 6){
    //   (this.languageCode === 'en')?this.registerErrMsg = "Password Should be minimum of 6 characters": 
    //   this.registerErrMsg = "Lozinka mora biti najmanje 6 znakova";
    // }
    else {
      this.loaderButton = false;
      (this.languageCode === 'en')?this.registerErrMsg = "field is missing": 
      this.registerErrMsg = "polje nedostaje";
      // this.registerErrMsg = "field is missing";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
  }

  logDevice() {
    var module = {
      options: [],
      header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor],
      dataos: [
        { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
        { name: 'Windows', value: 'Win', version: 'NT' },
        { name: 'iPhone', value: 'iPhone', version: 'OS' },
        { name: 'iPad', value: 'iPad', version: 'OS' },
        { name: 'Kindle', value: 'Silk', version: 'Silk' },
        { name: 'Android', value: 'Android', version: 'Android' },
        { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
        { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
        { name: 'Macintosh', value: 'Mac', version: 'OS X' },
        { name: 'Linux', value: 'Linux', version: 'rv' },
        { name: 'Palm', value: 'Palm', version: 'PalmOS' }
      ],
      databrowser: [
        { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
        { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
        { name: 'Safari', value: 'Safari', version: 'Version' },
        { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
        { name: 'Opera', value: 'Opera', version: 'Opera' },
        { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
        { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
      ],
      init: function () {
        var agent = this.header.join(' '),
          os = this.matchItem(agent, this.dataos),
          browser = this.matchItem(agent, this.databrowser);

        return { os: os, browser: browser };
      },
      matchItem: function (string, data) {
        var i = 0,
          j = 0,
          html = '',
          regex,
          regexv,
          match,
          matches,
          version;

        for (i = 0; i < data.length; i += 1) {
          regex = new RegExp(data[i].value, 'i');
          match = regex.test(string);
          if (match) {
            regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
            matches = string.match(regexv);
            version = '';
            if (matches) { if (matches[1]) { matches = matches[1]; } }
            if (matches) {
              matches = matches.split(/[._]+/);
              for (j = 0; j < matches.length; j += 1) {
                if (j === 0) {
                  version += matches[j] + '.';
                } else {
                  version += matches[j];
                }
              }
            } else {
              version = '0';
            }
            return {
              name: data[i].name,
              version: parseFloat(version)
            };
          }
        }
        return { name: 'unknown', version: 0 };
      }
    };

    var e = module.init(),
      debug = '';

    let list = {
      token: this._conf.getItem('authToken'),
      username: this._conf.getItem('username'),
      deviceName: e.os.name,
      deviceOs: e.os.version,
      appVersion: e.browser.version,
      deviceType: 3,
    }
    // console.log(list);
    this.homeService.logDeviceYelo(list)
      .subscribe((res) => {
        if (res.code == 200) {
          // this._router.navigate([""]);
          // window.location.replace('');
        }
      });
  }


  deviceId() {
    var module = {
      options: [],
      header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor],
      dataos: [
        { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
        { name: 'Windows', value: 'Win', version: 'NT' },
        { name: 'iPhone', value: 'iPhone', version: 'OS' },
        { name: 'iPad', value: 'iPad', version: 'OS' },
        { name: 'Kindle', value: 'Silk', version: 'Silk' },
        { name: 'Android', value: 'Android', version: 'Android' },
        { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
        { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
        { name: 'Macintosh', value: 'Mac', version: 'OS X' },
        { name: 'Linux', value: 'Linux', version: 'rv' },
        { name: 'Palm', value: 'Palm', version: 'PalmOS' }
      ],
      databrowser: [
        { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
        { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
        { name: 'Safari', value: 'Safari', version: 'Version' },
        { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
        { name: 'Opera', value: 'Opera', version: 'Opera' },
        { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
        { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
      ],
      init: function () {
        var agent = this.header.join(' '),
          os = this.matchItem(agent, this.dataos),
          browser = this.matchItem(agent, this.databrowser);

        return { os: os, browser: browser };
      },
      matchItem: function (string, data) {
        var i = 0,
          j = 0,
          html = '',
          regex,
          regexv,
          match,
          matches,
          version;

        for (i = 0; i < data.length; i += 1) {
          regex = new RegExp(data[i].value, 'i');
          match = regex.test(string);
          if (match) {
            regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
            matches = string.match(regexv);
            version = '';
            if (matches) { if (matches[1]) { matches = matches[1]; } }
            if (matches) {
              matches = matches.split(/[._]+/);
              for (j = 0; j < matches.length; j += 1) {
                if (j === 0) {
                  version += matches[j] + '.';
                } else {
                  version += matches[j];
                }
              }
            } else {
              version = '0';
            }
            return {
              name: data[i].name,
              version: parseFloat(version)
            };
          }
        }
        return { name: 'unknown', version: 0 };
      }
    };

    var e = module.init(),
      debug = '';

    this.deviceOs = e.os.name;
    this.otpSend(0);
  }
  disabledOtp:boolean = false;

  otpSend(val) {
    if(val == 1){
      this.disabledOtp = true;
      this.CountDownTimer();
    }
    console.log("the val is:",val)
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      // deviceId: this.deviceOs,
      deviceId: this.phonenumber,
      phoneNumber: "+" + flag.dialCode + this.phonenumber
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    let fullname = $("#fullname").val();
    let username = $("#username").val();
    let email = $("#email").val();
    let phone = $("#phone").val();
    let signPwd = $("#signPwd").val();
    console.log("the details for user :", fullname,username,email,phone,signPwd)
    if (fullname && username && email && phone && signPwd && signPwd.length >= 6) {
      this.otpToggle();
      this.homeService.otpCheck(list)
        .subscribe((res) => {
          console.log("the res is :",res);
          this.PhoneNum= list.phoneNumber;
          console.log("the phonenumber is :", this.PhoneNum);
          this.loaderButton = false;
          if (res.code == 200) {
            this.registerErrMsg = '';
          }
        });
    } 
    else if(this.password.length < 6){
      (this.languageCode === 'en')?this.registerErrMsg = "Password Should be minimum of 6 characters": 
      this.registerErrMsg = "Lozinka mora biti najmanje 6 znakova";
    }else {
      this.loaderButton = false;
      (this.languageCode === 'en')?this.registerErrMsg = "Field is Missing": 
      this.registerErrMsg = "Polje je nestalo";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
    setTimeout(() => {
      this.disabledOtp = false;
    }, 180000);
  }

  otpVerify() {
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      otp: this.otpNumber,
      deviceId: this.deviceOs,
      phoneNumber: "+" + flag.dialCode + this.phonenumber
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    this.homeService.verifyOtp(list)
      .subscribe((res) => {
        this.loaderButton = false;
        if (res.code == 200) {
          if(this.social == 1){
            this.fbRegisterlogin();
          }else if(this.social == 2){
            this.googleRegister();
          }else{
          this.register();
          }
        } else {
          this.loaderButton = false;
          this.registerErrMsg = res.message;
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
        }
      });

  }


  fileSignupUploader() {
    this.uploaders.uploadAll();
  }

  submitSignup(url) {
    console.log("uuuuuuuu", url);
    this.singnImg = url;
  }

  register() {
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      username: this.username,
      fullName: this.fullName,
      email: this.email,
      phoneNumber: "+" + flag.dialCode + this.phonenumber,
      password: this.password,
      signupType: 1,
      deviceType: 3,
      deviceId: "",
      pushToken: "",
      location: "",
      latitude: "",
      longitude: "",
      profilePicUrl: ""
    }
    if (this.singnImg) {
      list.profilePicUrl = this.singnImg;
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    if (this.username && this.fullName && this.email && this.phonenumber && this.password) {
      this.homeService.registerYelo(list)
        .subscribe((res) => {
          // this.loaderButton = false;
          if (res.code == 200) {
            this._conf.setItem('authToken', res.response.authToken);
            this._conf.setItem('email', res.response.email);
            this._conf.setItem('username', res.response.username);
            this._conf.setItem('userId', res.response.userId);
            this._conf.setItem('mqttId', res.response.mqttId);
            this._conf.setItem('profilePicUrl', res.response.profilePicUrl);
            this.registerValue();
            this.registerErrMsg = false;
            this.logDevice();
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            $(".modal").modal("hide");
            this._router.navigate([""]);
            // window.location.replace('');
          } else {
            this.loaderButton = false;
            this.registerErrMsg = res.message;
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 3000);
          }
        });
    } else {
      this.loaderButton = false;
      this.registerErrMsg = "field is missing";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
  }

  emailValidationCheck(val) {
    this.emailValidation(val);
    let list = {
      email: val
    }
    this.homeService.emailCheck(list)
      .subscribe((res) => {
        if (res.code != 200) {
          // alert(res.message);
          this.registerEmailErrMsg = res.message;
          this.email = "";
          setTimeout(() => {
            this.registerEmailErrMsg = false;
          }, 3000);
        } else {
          this.registerEmailErrMsg = false;
        }
      });
  }

  usernameCheck(val) {
    let list = {
      username: val
    }
    this.homeService.userNameCheck(list)
      .subscribe((res) => {
        if (res.code != 200) {
          // alert(res.message);
          this.registerUserErrMsg = res.message;
          this.username = "";
          setTimeout(() => {
            this.registerUserErrMsg = false;
          }, 3000);
        } else {
          this.registerUserErrMsg = false;
        }
      });
  }

  phonenumberCheck(val) {
    this.mobileValidation(val);
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      phoneNumber: "+" + flag.dialCode + val
    }
    this.homeService.phoneNumberCheck(list)
      .subscribe((res) => {
        if (res.code != 200) {
          // alert(res.message);
          this.registerPhoneErrMsg = res.message;
          this.phonenumber = "";
          setTimeout(() => {
            this.registerPhoneErrMsg = false;
          }, 3000);
        } else {
          this.registerPhoneErrMsg = false;
        }
      });
  }

  registerValue() {
    this.fullName = "";
    this.username = "";
    this.email = "";
    this.phonenumber = "";
    this.password = "";
    this.otpNumber = "";
  }

  emailValidation(value) {
    this.registerErrMsg = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.registerEmailErrMsg = false;
        this.registerSave = true;
      } else {
        if(this.languageCode == 'sr' || this.languageCode == 'srp'){
          this.registerEmailErrMsg = "email is incorrect";
        }
        else{
          this.registerEmailErrMsg = "pogrešan e-mail";
        }
        this.registerSave = false;
      }
    }
  }

  mobileValidation(value) {
    this.registerErrMsg = false;
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;

    if (value.match(regexPhone)) {
      this.registerPhoneErrMsg = false;
      this.phone_Error = false;
      this.registerSave = true;
    } else {
      var val = value.replace(/([^+0-9]+)/gi, '');
      this.phonenumber = val;
      this.phone_Error = true;
      this.registerPhoneErrMsg = "phone number is incorrect";
      this.registerSave = false;
    }
  }


  // onFacebookLoginClick() {   
  //   this.sub = this._auth.login("facebook").subscribe(
  //     (result) => {
  //       // console.log(result);
  //       this.me();
  //       // this.otp_Toggle=true;
  //       // 
  //     }
  //   )
  // }
  signInWithGoogle(): void {
    this._auth.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => 
      {
        console.log("the google user",x);
        this.onGoogle();
      });
    // this._auth.signIn(GoogleLoginProvider.PROVIDER_ID);
    // this.onGoogle();
  }

  signInWithFB(): void {
    this._auth.signIn(FacebookLoginProvider.PROVIDER_ID).then(x =>
       {
         console.log("the facebook user ",x); 
         this.me();
        });
    // this._auth.signIn(FacebookLoginProvider.PROVIDER_ID);
  }


  me() {
    this.resetValue();
    FB.api('/me?fields=id,picture,name,first_name,email,gender',
      (result: any) => {
        // console.log(result);        
        let list = {
          facebookId: result.id,
          email: result.email,
          profilePicUrl: "",
          loginType: 2,
          pushToken: "",
          place: "",
          city: "",
          countrySname: "",
          latitude: "",
          longitude: ""
        }
        if (result.picture.data.url) {
          list.profilePicUrl = result.picture.data.url;
        } else {
          list.profilePicUrl = "";
        }
        $(".loadSocial").show();
        setTimeout(() => {
          $(".loadSocial").hide();
        }, 3000);
        this.homeService.loginYelo(list)
          .subscribe((res) => {
            if (res.code == 200) {
              $(".loadSocial").hide();
              this._conf.setItem('authToken', res.token);
              this._conf.setItem('email', res.email);
              this._conf.setItem('username', res.username);
              this._conf.setItem('userId', res.userId);
              this._conf.setItem('profilePicUrl', res.profilePicUrl);
              this._conf.setItem('mqttId', res.mqttId);
              this.registerErrMsg = false;
              this.logDevice();
              this.missionService.confirmheaderRefresh(this.headerRefresh);
              // this._router.navigate([""]);
              window.location.replace('');
            } else {
              this.registerFb();
              $("#fullname").val(result.name);
              $("#email").val(result.email);
              $(".signupClick").trigger("click");
              $(".signupRegister").hide();
              $(".signUpFb").show();             
            }
          });
      });
  }

  social:any;
  
  registerFb() {
    this.social = 1;
    FB.getLoginStatus((response: any) => {
      if (response) {
        this.accessToken = response.authResponse.accessToken;
        console.log("test", this.accessToken);
      }
    });

    FB.api('/me?fields=id,picture,name,first_name,email,gender',
      (result: any) => {
        // console.log(result); 
        this.profilePicUrl = result.picture.data.url;
        this.accessToken = this.accessToken;

      });
  }

  fbRegisterlogin() {
    let fullname = $("#fullname").val();
    let email = $("#email").val();
    if (fullname) {
      this.registerfullErrMsg = false;
      let list = {
        accessToken: this.accessToken,
        username: this.username,
        fullName: fullname,
        email: email,
        phoneNumber: this.phonenumber,
        password: this.password,
        profilePicUrl: this.profilePicUrl,
        signupType: 2,
        deviceType: 3,
        deviceId: "",
        pushToken: "pushToken",
        location: "",
        latitude: "",
        longitude: ""
      }
      this.loaderButton = true;
      setTimeout(() => {
        this.loaderButton = false;
      }, 3000);
      this.homeService.registerYelo(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.loaderButton = false;
            this._conf.setItem('authToken', res.response.authToken);
            this._conf.setItem('email', res.response.email);
            this._conf.setItem('username', this.username);
            this._conf.setItem('userId', res.response.userId);
            this._conf.setItem('profilePicUrl', this.profilePicUrl);
            this._conf.setItem('mqttId', res.response.mqttId);
            this.registerErrMsg = false;
            this.logDevice();
            this.registerValue();
            this.missionService.confirmheaderRefresh(this.headerRefresh);           
            window.location.replace('');
          } else {
            this.registerErrMsg = res.message;
            this.loaderButtonfb = false;
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 5000);
          }
        });
    } else {
      this.registerfullErrMsg = true;
      setTimeout(() => {
        this.registerfullErrMsg = false;
      }, 5000);
    }
  }
    
  onGoogle() {
    this._auth.authState.subscribe((user) => {
      var googleUser = user;
      console.log("test", user);
      if (user && user.provider == 'GOOGLE') {
        this._zone.run(() => {
          let list = {
            googleId: googleUser.id,
            email: googleUser.email,
            profilePicUrl: "",
            loginType: 3,
            pushToken: "",
            place: "",
            city: "",
            countrySname: "",
            latitude: "",
            longitude: ""
          }
          if (googleUser.photoUrl) {
            list.profilePicUrl = googleUser.photoUrl;
          } else {
            list.profilePicUrl = "";
          }
          $.getJSON("https://freegeoip.net/json/", (data) => {
            list.city = data.city;
            list.countrySname = data.country_name;
            list.latitude = String(data.latitude);
            list.longitude = String(data.longitude);
          });
          // console.log(list);

          $(".loadgoogleSocial").show();
          setTimeout(() => {
            $(".loadgoogleSocial").hide();
          }, 3000);
          this.homeService.loginYelo(list)
            .subscribe((res) => {
              if (res.code == 200) {
                this.IsSelling = res.isSelling;
                $(".loadgoogleSocial").hide();
                this._conf.setItem('authToken', res.token);
                this._conf.setItem('email', res.email);
                this._conf.setItem('username', res.username);
                this._conf.setItem('userId', res.userId);
                this._conf.setItem('profilePicUrl', res.profilePicUrl);
                this._conf.setItem('mqttId', res.mqttId);
                this.registerErrMsg = false;
                this.logDevice();
                this.missionService.confirmheaderRefresh(this.headerRefresh);
                // this._router.navigate([""]);
                window.location.replace('');
              } else {
                $("#fullname").val(googleUser.name);
                $("#email").val(googleUser.email);
                $("#googleId").val(googleUser.id);
                $("#googleToken").val(googleUser.idToken);
                $(".signupClick").trigger("click");
                $(".ls").hide();
                $(".signUpgoogle").show();
              }
            });
        });
      }
    });

  }
  // onGoogleLoginClick() {
  //   console.log("the test for google login");
  //   this.sub = this._auth.login("google").subscribe(
  //     (googleUser) => {
  //       console.log("googleUser", googleUser);
  //       this.onSuccess(googleUser);
  //       // this.otp_Toggle=true;
  //     },error=>{
  //       console.log("the error for google login");
  //     }
  //   )
  // }

  onSuccess = (googleUser) => {
    console.log("the test for google login", googleUser)
    // this._zone.run(() => {
      let list = {
        googleId: googleUser.uid,
        email: googleUser.email,
        profilePicUrl: "",
        loginType: 3,
        pushToken: "",
        place: "",
        city: "",
        countrySname: "",
        latitude: "",
        longitude: ""
      }
      if (googleUser.image) {
        console.log("the google login 4")
        list.profilePicUrl = googleUser.image;
      } else {
        console.log("the google login 5")
        list.profilePicUrl = "";
      }
      $.getJSON("https://freegeoip.net/json/", (data) => {
        console.log("the google login 1")
        list.city = data.city;
        list.countrySname = data.country_name;
        list.latitude = String(data.latitude);
        list.longitude = String(data.longitude);
      });
      console.log(list);

      $(".loadgoogleSocial").show();
      setTimeout(() => {
        $(".loadgoogleSocial").hide();
      }, 3000);
      this.homeService.loginYelo(list)
        .subscribe((res) => {
          if (res.code == 200) {
            console.log("the google login 2")
            $(".loadgoogleSocial").hide();
            this._conf.setItem('authToken', res.token);
            this._conf.setItem('email', res.email);
            this._conf.setItem('username', res.username);
            this._conf.setItem('userId', res.userId);
            this._conf.setItem('profilePicUrl', res.profilePicUrl);
            this._conf.setItem('mqttId', res.mqttId);
            this.registerErrMsg = false;
            console.log("the test for google login success")
            this.logDevice();
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            // this._router.navigate([""]);
            window.location.replace('');
          } else {
            console.log("the google login 3")
            $("#fullname").val(googleUser.name);
            $("#email").val(googleUser.email);
            $("#googleId").val(googleUser.uid);
            $("#googleToken").val(googleUser.idToken);
            $(".signupClick").trigger("click");
            $(".ls").hide();
            $(".signUpgoogle").show();
            this.social = 2;
            console.log("the test for google login fail")
          }
        });
    // });

  }


  googleRegister() {
    let fullname = $("#fullname").val();
    let email = $("#email").val();
    let googleId = $("#googleId").val();
    let googleToken = $("#googleToken").val();
    if (fullname) {
      this.registerfullErrMsg = false;
      let list = {
        googleToken: googleToken,
        googleId: googleId,
        username: this.username,
        fullName: fullname,
        email: email,
        phoneNumber: this.phonenumber,
        password: this.password,
        profilePicUrl: this.profilePicUrl,
        signupType: 3,
        deviceType: 3,
        deviceId: "",
        pushToken: "pushToken",
        location: "",
        latitude: "",
        longitude: ""
      }

      this.loaderButton = true;
      setTimeout(() => {
        this.loaderButton = false;
      }, 3000);
      this.homeService.registerYelo(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.loaderButton = false;
            this._conf.setItem('authToken', res.response.authToken);
            this._conf.setItem('email', res.response.email);
            this._conf.setItem('username', this.username);
            this._conf.setItem('userId', res.response.userId);
            this._conf.setItem('profilePicUrl', this.profilePicUrl);
            this._conf.setItem('mqttId', res.response.mqttId);
            this.registerErrMsg = false;
            this.logDevice();
            this.registerValue();
            // this._conf.setItem("google", '1');
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            // this._router.navigate([""]);
            window.location.replace('');
          } else {
            this.registerErrMsg = res.message;
            this.loaderButtonfb = false;
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 5000);
          }
        });
    } else {
      this.registerfullErrMsg = true;
      setTimeout(() => {
        this.registerfullErrMsg = false;
      }, 5000);
    }
  }


  onFailure(error) {
    console.log(error);
  }


  changeLocation() {
    var input = document.getElementById('sellLocate');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === 'country') {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
        }
      }
      // console.log("cityArea", place);
      if (City) {
        $("#cityS").val(City);
      } else if (state) {
        $("#cityS").val(state);
      } else {
        $("#cityS").val(place.formatted_address);
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      $("#latS").val(lat);
      $("#lngS").val(lng);
      $("#state").val(state);
      $("#locate").val(place.formatted_address);
    });
  }
  valueArr =[];
  valueToshow = '';
  addOnlyNumComma(event){
      // const pattern = /^[0-9,]*$/;  
      const pattern = /^[0-9]*[,]{0,1}[0-9]{0,2}$/;
      this.valueToshow = event.target.value;
      if(!pattern.test(event.target.value)){
        event.target.value = event.target.value.replace(/[^0-9,]/g, '').replace(/(\..*)\./g, '$1');
        if(this.valueToshow.lastIndexOf(',') > -1){
          this.valueArr = this.valueToshow.split(',');
          console.log("the valuetoShow is:", this.valueArr);
          (this.valueArr.length == 1)? this.valueToshow = this.valueArr[0]: this.valueToshow = this.valueArr[0]+','+ this.valueArr[1].slice(0,2);
          event.target.value = event.target.value.replace(event.target.value, this.valueToshow)
        }
        console.log("the mismatched");
      }
      // else{
      //   console.log("the matched")
      // }
      // this.valueToshow = event.target.value;
      // console.log("the productt price is:",this.productPrice, this.productPrice.length );
      // (!pattern.test(event.target.value)) ? (
      //   // event.target.value = event.target.value.replace(/[^0-9,]/g, '').replace(/(\..*)\./g, '$1'),
       
      //   event.target.value = event.target.value.replace(/[^0-9]*[^,]{0,1}[^0-9]{0,2}/g, '').replace(/(\..*)\./g, '$1'),
      //   (event.target.value && event.target.value.length > 0 )? console.log("the string not is empty") : console.log("the string is empty"),
      //   console.log("the pattern mismatched")
      // ) :(
      //   console.log("the mismatched"))
  }

  selectedLanguage(languageValue){

    console.log("the slected value :",typeof languageValue );
    sessionStorage.setItem("Language",languageValue)
    switch(languageValue){
     
      case "1":this.header = this._lang.engHeader1;
      // this.placelatlng = {
      //   'lat':'44.0165',
      //   'lng':'21.0059'
      // }
      // this._conf.setItem('latlng',JSON.parse(this.placelatlng));
            break; 
      case '2':this.header = this._lang.engHeader1;
      // this.placelatlng = {
      //   'lat':'42.7087',
      //   'lng':'19.3744'
      // }
      // this._conf.setItem('latlng',JSON.parse(this.placelatlng));
             break;

      case '3':  this.header = this._lang.engHeader;
              break;
    }
    // sessionStorage.setItem("Languages",'1');
    // this._router.navigate(['./about']);
    window.location.reload();
  }
}
