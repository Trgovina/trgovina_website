import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { LanguageService } from '../app.language';
import { Configuration } from '../app.constants';
declare var $: any;

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  footerClosed = false;
  succesSeller = false;
  footer:any;

  constructor(private missionService: MissionService, private _lang: LanguageService,private _conf: Configuration,) {
    missionService.missionheaderOpen$.subscribe(
      headerOpen => {
        this.footerClosed = true;
      });
    missionService.missionheaderClose$.subscribe(
      headerClose => {
        this.footerClosed = true;
      });
    missionService.missionheaderClosed$.subscribe(
      headerClosed => {
        this.footerClosed = false;
      });
    missionService.missionheaderHelpClose$.subscribe(
      headerHelpClose => {
        this.footerClosed = true;
      });
    
  }

  ngOnInit() {
  //  let selectedLang  = Number(this._conf.getItem("Language"));
   let selectedLang  = Number(sessionStorage.getItem("Language"));
    if(selectedLang){
      switch(selectedLang){

        case 1: case 2:  this.footer = this._lang.engFooter1;
                          break;

        case 3:  this.footer = this._lang.engFooter;
                break;
      }
    }
    else{
      this.footer = this._lang.engFooter1;
    }
    // this.footer = this._lang.engFooter;
  }

}
