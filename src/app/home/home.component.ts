import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MissionService } from '../app.service';
import { HomeService } from './home.service';
import { Configuration } from '../app.constants';
import { Meta } from '@angular/platform-browser';
import { LanguageService } from '../app.language';

declare var $: any;
declare var google: any;

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _missionService: MissionService,
    private _meta: Meta,
    private _conf: Configuration,
    private _router: Router,
    private _service: HomeService,
    private _lang: LanguageService,
  ) {
    _missionService.missionCatName$.subscribe(
      catName => {
        console.log(catName, this.categoryVal)
        if (catName == "1") {
          // if (this.categoryVal.length > 0) {
          //   this.searchCategory();
          // } else {
          $('.radioClass').removeAttr('checked');
          this.categoryVal = [];
          this.catList.forEach(x => {
            x.activeimg = false;
          })
          this.allListPosts();
          // }
        } else {
          console.log("the category is searched :", catName);
          this.categoryName = catName;
          if(this.categoryName.length > 0){
            this.searchCategory();
            this.searchItem = 0;
          }
          else{
            this.allListPosts();
            this.bannerShow = false;
            this.searchItem = 1;
          }
          // this.categoryName = catName;
          // this.searchCategory();
          // this.searchItem = 0;
        }
      });
  }
  headerClose: string;
  token: string;
  offset = 0;
  limit = 40;
  allList: any = [];
  allListEmpty: any = [];
  catList: any;
  catSelectList = false;
  subCat: any;
  catSubFilter: any;
  loadMoreList = false;
  loadMoreListArrow = false;
  allListData = false;
  placelatlng: any;
  recentCity: any;
  listLength: any;
  listsLength = false;
  offsetImg = 100;
  defaultImage = 'assets/images/lazyLoad.png';
  sellEmail: string;
  sellPhone: string;
  email_Error = false;
  phone_Error = false;
  registerSave = false;
  PEerror: any;
  succesSeller = false;
  howPopupSuccess = false;
  listSell: any;
  signUpContent: boolean = true;
  langaugeCode: string ='rs';
  filterPrice = false;
  categoryName: any;
  latlng: any;
  priceOrder = "lowToHigh";
  timeOrder = 0;
  distance = 3000;
  minPrice: any;
  maxPrice: any;
  filterScroll: any;
  postedWithin: any;
  currency: any;
  DefualtCurrency:any;
  bannerShow = false;
  loadCateg = false;

  home: any;
  data: any;

  infowindow: any;
  map: any;
  place: any;

  shimmer: any = [];
  searchItem = 0;
  IpinfoData:any;
  categoryVal = [];
  catArray = [];
  selectCatList = [];
  selectedCategoryNodeId:any;
  ngOnInit() {
    // sessionStorage.clear();
    this._conf.removeItem("CountryCode");
    // this.currentLocation();
    // $.get("https://ipapi.co/json", (response)=> {
    //   console.log(response);
    //   sessionStorage.setItem("CountryCode",response.country);
    // });
   
    console.log("the coutry is spain");
   // let selectedLang  = Number(this._conf.getItem("Language"));
   let selectedLang  = Number(sessionStorage.getItem("Language"));
    if(selectedLang){
      switch(selectedLang){

        case 1: this.home = this._lang.engHome1;
                this.langaugeCode = 'sr';
                // this.placelatlng = {
                //   'lat':'44.0165',
                //   'lng':'21.0059'
                // }
                // this._conf.setItem('latlng',JSON.parse(this.placelatlng));
                break; 
        case 2:this.home = this._lang.engHome1;
               this.langaugeCode = 'srp';
              //  this.placelatlng = {
              //     'lat':'42.7087',
              //     'lng':'19.3744'
              //   }
              //   this._conf.setItem('latlng',JSON.parse(this.placelatlng));
                break;

        case 3:  this.home = this._lang.engHome;
                 this.langaugeCode ='en';
                break;
      }
    }
    else{
      this.home = this._lang.engHome1;
      this.langaugeCode = 'sr';
    }
    // this.home = this._lang.engHome;
  
  

    let urlPath = this._conf.getItem("pathname");
    if (urlPath) {
      let list = urlPath.split("/");
      var index = list.indexOf("website");
      if (index > -1) {
        list.splice(index, 1);
      }
      console.log(list.join('/'));
      let url = list.join('/');
      this._router.navigate([url]);
      this._conf.removeItem("pathname");
    } else {

      this.seo();
      this.placeHolder();

      $(window).on('popstate', function () {
        $('.modal').modal('hide');
      });

      this._missionService.confirmheaderClose(this.headerClose);
      $(".location").text("Change Location");
      console.log("the auth token is :",  this.token );
      this.token = this._conf.getItem('authToken');
      let latlng = this._conf.getItem('latlng');
      let cityArea = this._conf.getItem('cityArea');
      console.log("the latlng is :",latlng );
      if (cityArea) {
        $("#cityAreai").val(cityArea);
      }
      if (!latlng) {
        this.currentLocation();
      }
      else{
        this.allListPosts();
      }
      this.listGetCategories();
      setTimeout(() => {
        let latlng1 = this._conf.getItem('latlng');
        console.log("the lat and lng :",latlng1)
        if (!latlng1 || !cityArea) {
          this.latLngNull();
        }
      }, 3000);

      // $.get("https://api.ipdata.co", (response) => {
      //   console.log("currency", response)
      //   this.currency = response.currency.code;
      // }, "jsonp");

      $.getJSON("https://restcountries.eu/rest/v1/alpha/in", (data) => {
         console.log("the currency code",data)
         this._conf.setItem("CountryCode", data.alpha2Code);
         console.log("the country code is :",this._conf.getItem("CountryCode" ));
        //  this.currency = data.currencies[0];
        this.DefualtCurrency= 'EUR';
        this.currency = 'EUR';
      });
    }
  }

  placeHolder() {
    for (var i = 0; i < 12; i++) {
      this.shimmer.push(i);
    }
  }

  public currencyList = [
    { value: "EUR", text:"montenegro" },
    { value: "DIN", text:"Serbian dinar"}
    // { value: "USD", text: "United States Dollars" },
    // { value: "EUR", text: "Euro" },
    // { value: "GBP", text: "United Kingdom Pounds" },
    // { value: "DZD", text: "Algeria Dinars" },
    // { value: "ARP", text: "Argentina Pesos" },
    // { value: "AUD", text: "Australia Dollars" },
    // { value: "ATS", text: "Austria Schillings" },
    // { value: "BSD", text: "Bahamas Dollars" },
    // { value: "BBD", text: "Barbados Dollars" },
    // { value: "BEF", text: "Belgium Francs" },
    // { value: "BMD", text: "Bermuda Dollars" },
    // { value: "BRR", text: "Brazil Real" },
    // { value: "BGL", text: "Bulgaria Lev" },
    // { value: "CAD", text: "Canada Dollars" },
    // { value: "CLP", text: "Chile Pesos" },
    // { value: "CNY", text: "China Yuan Renmimbi" },
    // { value: "CYP", text: "Cyprus Pounds" },
    // { value: "CSK", text: "Czech Republic Koruna" },
    // { value: "DKK", text: "Denmark Kroner" },
    // { value: "NLG", text: "Dutch Guilders" },
    // { value: "XCD", text: "Eastern Caribbean Dollars" },
    // { value: "EGP", text: "Egypt Pounds" },
    // { value: "FJD", text: "Fiji Dollars" },
    // { value: "FIM", text: "Finland Markka" },
    // { value: "FRF", text: "France Francs" },
    // { value: "DEM", text: "Germany Deutsche Marks" },
    // { value: "XAU", text: "Gold Ounces" },
    // { value: "GRD", text: "Greece Drachmas" },
    // { value: "HKD", text: "Hong Kong Dollars" },
    // { value: "HUF", text: "Hungary Forint" },
    // { value: "ISK", text: "Iceland Krona" },
    // { value: "INR", text: "India Rupees" },
    // { value: "IDR", text: "Indonesia Rupiah" },
    // { value: "IEP", text: "Ireland Punt" },
    // { value: "ILS", text: "Israel New Shekels" },
    // { value: "ITL", text: "Italy Lira" },
    // { value: "JMD", text: "Jamaica Dollars" },
    // { value: "JPY", text: "Japan Yen" },
    // { value: "JOD", text: "Jordan Dinar" },
    // { value: "KRW", text: "Korea(South) Won" },
    // { value: "LBP", text: "Lebanon Pounds" },
    // { value: "LUF", text: "Luxembourg Francs" },
    // { value: "MYR", text: "Malaysia Ringgit" },
    // { value: "MXP", text: "Mexico Pesos" },
    // { value: "NLG", text: "Netherlands Guilders" },
    // { value: "NZD", text: "New Zealand Dollars" },
    // { value: "NOK", text: "Norway Kroner" },
    // { value: "PKR", text: "Pakistan Rupees" },
    // { value: "XPD", text: "Palladium Ounces" },
    // { value: "PHP", text: "Philippines Pesos" },
    // { value: "XPT", text: "Platinum Ounces" },
    // { value: "PLZ", text: "Poland Zloty" },
    // { value: "PTE", text: "Portugal Escudo" },
    // { value: "ROL", text: "Romania Leu" },
    // { value: "RUR", text: "Russia Rubles" },
    // { value: "SAR", text: "Saudi Arabia Riyal" },
    // { value: "XAG", text: "Silver Ounces" },
    // { value: "SGD", text: "Singapore Dollars" },
    // { value: "SKK", text: "Slovakia Koruna" },
    // { value: "ZAR", text: "South Africa Rand" },
    // { value: "KRW", text: "South Korea Won" },
    // { value: "ESP", text: "Spain Pesetas" },
    // { value: "XDR", text: "Special Drawing Right(IMF)" },
    // { value: "SDD", text: "Sudan Dinar" },
    // { value: "SEK", text: "Sweden Krona" },
    // { value: "CHF", text: "Switzerland Francs" },
    // { value: "TWD", text: "Taiwan Dollars" },
    // { value: "THB", text: "Thailand Baht" },
    // { value: "TTD", text: "Trinidad and Tobago Dollars" },
    // { value: "TRL", text: "Turkey Lira" },
    // { value: "VEB", text: "Venezuela Bolivar" },
    // { value: "ZMK", text: "Zambia Kwacha" },
    // { value: "EUR", text: "Euro" },
    // { value: "XCD", text: "Eastern Caribbean Dollars" },
    // { value: "XDR", text: "Special Drawing Right(IMF)" },
    // { value: "XAG", text: "Silver Ounces" },
    // { value: "XAU", text: "Gold Ounces" },
    // { value: "XPD", text: "Palladium Ounces" },
    // { value: "XPT", text: "Platinum Ounces" },
    // { value: "QAR", text:"Qatari Riyal"}
  ]


  latLngNull() {
    $.get("https://ipapi.co/json", (response)=> {
      console.log(response);
      console.log("the response for ipinfo",response);
      this._conf.setItem('country',response.country);
      sessionStorage.setItem("CountryCode",response.country);
      // let ipList = response.loc.split(",");
      if(response.latitude == '42.0931')
      {
        response.latitude = '42.4411';
      }
      if(response.longitude == '19.2636'){
        response.longitude = '19.2636';
      }
      this.placelatlng = {
        'lat':  response.latitude,
        'lng':  response.longitude
      }
      this.IpinfoData  = response;
      let latlng1 = this._conf.getItem('latlng');
      if (!latlng1) {
        console.log("json", this.placelatlng);
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this.allListPosts();
      }
      this.recentCity = response.city + ", " + response.country;
      this._conf.setItem('recentCity', this.recentCity);
      this._conf.setItem('city', response.city);
      this._conf.setItem('country', response.country);
      this._conf.setItem('lat',  response.latitude);
      this._conf.setItem('lng', response.longitude);
      $("#cityAreai").val(this.recentCity);
      $(".location").text(this.recentCity);
  });
  }

  seo() {
    let list = {
      type: 1
    }
    this._service.seoList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.data = res.data;
          // console.log("desc", res.data);
          if (this.data) {
            this._meta.addTag({ name: 'title', content: this.data.title })
            this._meta.addTag({ name: 'description', content: this.data.description })
            this._meta.addTag({ name: 'keywords', content: this.data.keyword.toString() })
          }
        }
      });
  }

  emptyFigure() {
    for (var i = 0; i < 20; i++) {
      this.allListEmpty.push(i);
      // console.log(this.allListEmpty);
    }
  }

  currentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        if(lat.toFixed(4).toString() == '42.0931')
        {
           lat = 42.4411;
        }
        if(lng.toFixed(4).toString() == '19.2636'){
          lng = 19.2636;
        }
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
        console.log("HomeLatLng", this.placelatlng);
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        let latlng = this._conf.getItem('latlng');
        if (latlng) {
          this.allListPosts();
        }
        var latilng = new google.maps.LatLng(lat, lng);
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latilng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (var i = 0; i < results[1].address_components.length; i += 1) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === 'locality') {
                    // console.log(addressObj.types[j]);
                    var city = addressObj.long_name;
                    // console.log(results[0].formatted_address)                   
                  }
                  if (addressObj.types[j] === 'country') {
                    // console.log(addressObj.types[j]);
                    var country = addressObj.short_name;
                    // console.log(results[0].formatted_address)                   
                  }
                }
              }
              $("#cityAreai").val(city + ", " + country);
              $(".location").text(city + ", " + country);
              this._conf.setItem('recentCity', city + ", " + country);
            }
            // console.log(results)
          }
        });
      }, this.geoError);
    } else {
      console.log("Geolocation is not supported by this browser.");
    }
  }

  geoError() {
    console.log("Geolocation is not supported by this browser.");
  }

  itemDetails(postId, name) {
    let replace = name.split(' ').join('-');
    // console.log(replace)
    this._router.navigate(["./", replace, postId]);
  }

  public bgcolor = [
    { 'bgcolor': 'rgb(134, 176, 222)' },
    { 'bgcolor': 'rgb(255, 63, 85)' },
    { 'bgcolor': 'rgb(115, 189, 197)' },
    { 'bgcolor': 'rgb(232, 111, 91)' },
    { 'bgcolor': 'rgb(166, 196, 136)' },
    { 'bgcolor': 'rgb(245, 205, 119)' },
    { 'bgcolor': 'rgb(190, 168, 210)' },
    { 'bgcolor': 'rgb(252, 145, 157)' },
    { 'bgcolor': 'rgb(83, 143, 209)' },
    { 'bgcolor': 'rgb(209, 169, 96)' },
    { 'bgcolor': 'rgb(227, 74, 107)' }
  ]

  listGetCategories() {
    this.catSelectList = false;
    this.selectCatList = [];
    this._service.getCategoriesList(this.langaugeCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
          // for (var i = 0; i < this.catList.length; i++) {
          //   this.catList[i].bgcolor = this.bgcolor[i % 11].bgcolor;
          // }
        }
      });
  }

  loadCat() {
    this.loadCateg = !this.loadCateg;
  }


  allListPosts() {
    console.log("the category search ")
    this.bannerShow = false;
    let list = {
      latitude: "",
      longitude: "",
      offset: 0,
      limit: this.limit
    }
    this.placelatlng = this._conf.getItem('latlng');
    console.log("the lat and lng is :", this.placelatlng)
    if (this.placelatlng) {
      this.placelatlng = JSON.parse(this.placelatlng);
      list.latitude = this.placelatlng.lat;
      list.longitude = this.placelatlng.lng;
    }

    if (this.offset == 0) {
      this.allListData = false;
      setTimeout(() => {
        this.allListData = true;
      }, 3000);
    }
    this._service.homeAllList(list)
      .subscribe((res) => {
        this.allListData = true;
        let allList = res.data;
        if (res.code == 200) {
          if (this.offset == 0) {
            this.allList = [];
          }
          let object = {};
          let result = [];

        this.allList.forEach(function (item,index) {
          let Item =  this.allList[index].postId
          if(!object[Item])
              object[Item] = 0;
            object[Item] += 1;
        })

        for (let prop in object) {
           if(object[prop] <= 2) {
               result.push(prop);
           }
        }
        this.allList = result;

          for (var i = 0; i < allList.length; i++) {
            this.allList.push(res.data[i]);
            // if(res.data[i].isPromoted == 1){
            //   console.log("isPromoted", res.data[i].isPromoted)
            // }
          }
          // this.allList.forEach((item,index)=>{
          //   let price = (item.price).toString();
           
          // });

          // this.allList.forEach((item,index)=>{
          //       if(this.allList.lastIndexOf(this.allList[index].postId) > -1){
          //         let indx = this.allList.lastIndexOf(this.allList[index].postId);
          //         this.allList.splice(indx, 1);
          //       }
          // })
          if (this.allList && this.allList.length > 0) {
            this.listsLength = false;
            // this.convertToEurCurrency(this.allList);
          } else {
            this.listsLength = true;
            this.allListData = true;
          }
          this.listLength = this.allList.length;
          this.filterScroll = 0;
        } else {
          this.listsLength = true;
          this.allListData = true;
        }
      });
  }
  onScroll() {
    if (this.listLength == 40 && this.filterScroll == 0) {
      this.offset += 40;
      console.log("allScroll", this.listLength);
      this.allListPosts();
    } else if (this.listLength == 40 && this.filterScroll == 1) {
      this.offset += 40;
      console.log("filterScroll", this.listLength);
      this.searchCategory();
    } else {
      console.log("error", this.listLength);
    }
  }

  // convertToEurCurrency(DataList){
  //   let convertedArray = [];
  //    DataList.forEach((item,index)=>{
  //        if(item.currency == 'EUR'){
  //           let price = item.price.toString();
  //           price.replace('.',',');
  //           item.price = parseInt(price)
  //           convertedArray.push({
  //             currency:item.currency,
  //             price: item.price
  //           })  
  //        }
  //    });
  //    console.log("the converted arrays for currencty are :",convertedArray);
  // }
  emailValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.email_Error = false;
        this.registerSave = true;
      } else {
        this.email_Error = true;
        this.registerSave = false;
      }
    } else {
      this.email_Error = false;
    }
  }

  mobileValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
      if (value.match(regexPhone)) {
        this.phone_Error = false;
        this.registerSave = true;
      } else {
        var val = value.replace(/([^+0-9]+)/gi, '')
        this.sellPhone = val;
        this.phone_Error = true;
        this.registerSave = false;
      }
      if (value.length == 10) {
        this.phone_Error = true;
        this.registerSave = false;
      }
    } else {
      this.phone_Error = false;
    }
  }

  sellHowSend() {
    console.log(this.sellEmail, this.sellPhone);
    if (this.sellEmail && this.sellPhone) {
      this.howPopupSuccess = false;
      this.phone_Error = false;
      this.email_Error = false;
      this.PEerror = "Please try anyone";
      setTimeout(() => {
        this.PEerror = false;
      }, 3000);
    } else if (this.sellEmail || this.sellPhone) {
      if (this.sellEmail) {
        this.sellSend(1);
      } else {
        this.sellSend(2);
      }
      this.PEerror = false;
    }
  }

  emptySell() {
    this.sellPhone = "";
    this.sellEmail = "";
    this.email_Error = false;
    this.phone_Error = false;
  }

  sellSend(val) {
    this.email_Error = false;
    this.phone_Error = false;
    if (val == 1) {
      this.listSell = {
        type: "1",
        emailId: this.sellEmail
      }
    } else {
      this.listSell = {
        type: "2",
        phoneNumber: this.sellPhone
      }
    }
    // console.log(this.listSell);
    this._service.sellList(this.listSell)
      .subscribe((res) => {
        if (res.code == 200) {
          this.howPopupSuccess = true;
          this.emptySell();
        } else {
          this.PEerror = res.message;
        }
      });
  }

  // changeLocation() {
  //   var input = document.getElementById('changeLocationId');
  //   var autocomplete = new google.maps.places.Autocomplete(input);

  //   autocomplete.addListener('place_changed', function () {
  //     var place = autocomplete.getPlace();
  //     for (var i = 0; i < place.address_components.length; i += 1) {
  //       var addressObj = place.address_components[i];
  //       for (var j = 0; j < addressObj.types.length; j += 1) {
  //         if (addressObj.types[j] === 'locality') {
  //           var City = addressObj.long_name;
  //           // console.log(addressObj.long_name);
  //         }
  //         if (addressObj.types[j] === 'administrative_area_level_1') {
  //           var state = addressObj.short_name;
  //           // console.log(addressObj.short_name);
  //         }
  //       }
  //     }
  //     // console.log("cityArea", City + ", " + state);
  //     if (City) {
  //       $("#cityAreai").val(City + ", " + state);
  //     } else if (state) {
  //       $("#cityAreai").val(state);
  //     } else {
  //       $("#cityAreai").val(place.formatted_address);
  //     }
  //     let lat = place.geometry.location.lat();
  //     let lng = place.geometry.location.lng();
  //     $("#lati").val(lat);
  //     $("#lngi").val(lng);
  //   });
  // }

  // locationChange() {
  //   setTimeout(() => {
  //     this.offset = 0;
  //     this.searchCategory();
  //   }, 500);
  // }

  milesDistance(val) {
    var input = $(".slider_range").val();
    var value = (input - $(".slider_range").attr('min')) / ($(".slider_range").attr('max') - $(".slider_range").attr('min'));
    $(".slider_range").css('background-image',
      '-webkit-gradient(linear, left top, right top, '
      + 'color-stop(' + value + ', #5c34a3), '
      + 'color-stop(' + value + ', #C5C5C5)'
      + ')'
    );
    this.offset = 0;
    if (val == 50) {
      val = 3000;
    }
    this.distance = val;
    if (val != 0) {
      // this.searchCategory();
    } else {
      this.allListPosts();
    }
  }

  signUpContentToggle() {
    this.signUpContent = !this.signUpContent;
  }

  postedWithinList(val, index) {
    this.postedWithin = val;
    if (val == 0) {
      $('.radioClass').removeAttr('checked');
      this.categoryVal = [];
      this.catList.forEach(x => {
        x.activeimg = false;
      })
     
      $("html, body").animate({ scrollTop: 0 }, 500);
      if (index == 0) {
        $("#sideDetailsId").removeClass('active');
        $("body").removeClass("hideHidden");
        this.allListPosts();
        this.listGetCategories();
        this.offset = 0;
      }
      // this.allListPosts();
    } else if (val == 10) {
      $('.radioClass').removeAttr('checked');
      $('.inputCheckBox').removeAttr('checked');
      $("#myRange").val(0);
      $(".slider_range").css('background' ,'#d3d3d3');
      $(".froms").val('');
      $(".tos").val('');
      $('#fromToPricingInput').val('');
      $('.fromToPricingInput').val('');
      $('.inputBox').val('');
      this.currency = this.DefualtCurrency;
      console.log("the reset currency are ,",this.DefualtCurrency,this.currency);
      console.log("the value for range is : ",$("#myRange").val() );
      this.categoryVal = [];
      this.catList.forEach(x => {
        x.activeimg = false;
      })
      this.subCat.forEach(x => {
        x.activeimg = false;
      })
    } else {
      this.offset = 0;
      this.postedWithin = val;
      console.log("the posted within", this.postedWithin);
      // this.searchCategory();
    }
    if(this.subCat){
      this.subCat.forEach(x => {
        x.activeimg = false;
      })
    }    
  }



  sortPrice(val) {
    this.offset = 0;
    this.priceOrder = val;
    console.log("the priceorder is :", this.priceOrder);
    // this.searchCategory();
  }

  numberDitchit(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
    var val = value.replace(/([^+0-9]+)/gi, '');
    $(".from1").val(val);
  }

  numberDitchit1(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
    var val = value.replace(/([^+0-9]+)/gi, '');
    $(".to1").val(val);
  }

  minimumPrice(val) {
    this.offset = 0;
    this.minPrice = val;
    // this.searchCategory();
  }

  maximumPrice(val) {
    this.offset = 0;
    this.maxPrice = val;
    // this.searchCategory();
  }

  selectCurrency(val) {
    this.currency = val;
    // this.searchCategory();
  }

  searchCategoryName(val, i, filter) {
    console.log("the category search ",val, i);
    $("#nameCategory").val("");
    // if (!this.catList[i].activeimg) {
    //   this.catList[i].activeimg = true;
    // } else {
    //   this.catList[i].activeimg = false;
    // }
    // this.categoryVal = [];
    // this.catList.forEach(x => {
    //   if (x.activeimg == true) {
    //     this.categoryVal.push(x.name.toLowerCase());
    //   } else {
    //     x.activeimg = false
    //   }
    // })
    this.selectCatList = [];
    this.catList.forEach(x => {
      x.activeimg = false
    })
    this.catList[i].activeimg = true;
    this.selectCatList.push(this.catList[i]);
    this.catSelectList = true;
    this.subCat = "";
    this.catSubFilter = "";
    this.subCategoryName = "";
    // let categoryVal = this.catList[i].name;
    // console.log(this.catArray);
    this.categoryName = this.catList[i].name;
    this.selectedCategoryNodeId = this.catList[i].categoryNodeId;
    this.filterPrice = false;
    this.offset = 0;    
    if (filter == 0) {
      console.log("the category search 000000");
      this.searchItem = 1;
      this.searchCategory();
    }
    this._service.getSubCategoriesList(this.catList[i].name,this.langaugeCode,this.selectedCategoryNodeId)
      .subscribe((res) => {
        this.subCat = res.data;
        console.log("the category sub, this,subCat",this.subCat)
      })

    if (this.categoryName == "") {
      this.postedWithinList(0, 1);
    }
  }

  cancelCat(i) {
    this.catList.forEach(x => {
      x.activeimg = false
    })
    this.subCat = "";
    this.listGetCategories();
    if (!$("#sideDetailsId").hasClass("active")) {
      this.postedWithinList(0, 0);
    }
  }

  subCategoryName: any;
  subCategoryNodeId: any;
  subCategory(i) {
    this.subCategoryName = this.subCat[i].subCategoryName;
    this.subCategoryNodeId = this.subCat[i].subCategoryNodeId;
    this.subCat.forEach(x => {
      x.activeimg = false
    })
    this.subCat[i].activeimg = true;
    this.selectCatList.splice(1, 1);
    this.selectCatList.push(this.subCat[i]);
    var value = [];
    this.subCat[i].filter.forEach(x => {
      if (x.type == 2 || x.type == 4 || x.type == 6) {
        x.filterData = x.values.split(",");
        x.data = [];
        // x.value = split;
      }
      value.push(x);
    });

    this.catSubFilter = value;
  }

  cancelsubCat() {
    this.subCat.forEach(x => {
      x.activeimg = false
    })
    this.catSubFilter = "";
    this.selectCatList.splice(1, 1);
  }

  cancelCats(i) {
    $("#nameCategory").val("");
    let len = this.selectCatList.length;
    if (i == 1) {
      console.log("the sub cat all lists")
      this.cancelsubCat();
    } else {
      console.log("the all lists")
      this.postedWithinList(0, 0);
    }
  }


  documentTextList(event, i, range) {
    // console.log("d", event.target.value.length)
    if (event.target.value.length > 0) {
      if (this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5) {
        if (range == 0) {
          this.catSubFilter[i].from = Number(event.target.value);
        } else {
          this.catSubFilter[i].to = Number(event.target.value);
        }
        this.catSubFilter[i].types = "range";
        this.catSubFilter[i].checked = true;
      } else if (this.catSubFilter[i].type == 2 || this.catSubFilter[i].type == 4 || this.catSubFilter[i].type == 6) {
        var index = this.catSubFilter[i].data.findIndex(x => x.value == this.catSubFilter[i].filterData[range]);
        if (index > -1) {
          this.catSubFilter[i].data.splice(index, 1)
        } else {
          let list = {
            type: "equlTo",
            fieldName: this.catSubFilter[i].fieldName,
            value: this.catSubFilter[i].filterData[range]
          }
          this.catSubFilter[i].data.push(list);
        }
        if (this.catSubFilter[i].data && this.catSubFilter[i].data.length) {
          this.catSubFilter[i].checked = true;
          this.catSubFilter[i].typed = 4;
        } else {
          this.catSubFilter[i].checked = false;
        }
      } else {
        this.catSubFilter[i].data = event.target.value;
        this.catSubFilter[i].types = "equlTo";
        this.catSubFilter[i].checked = true;
      }
    } else {
      this.catSubFilter[i].checked = false;
    }

  }

  distances: any;
  errMsg: any;
  loaderButton: any;
  distanceList(val) {
    this.distances = Number(val);
  }

  filterToggle() {
    // $("html, body").animate({ scrollTop: 0 }, 500);
    // $("#sideDetailsId").toggleClass('active');
    // $("body").toggleClass("hideHidden");
    this.searchItem = 1;
    this.searchCategory();
    // this.searchItem = 1;
    // this.postedWithinList(0, 0);
  }

  searchCategory() {
    this.errMsg = false;
    setTimeout(() => {
      this.loaderButton = false;
      this.errMsg = false;
    }, 3000);
    var filData = [];
    var filDatas = [];
    if (this.catSubFilter) {
      this.catSubFilter.forEach(x => {
        if (x.checked == true && !x.typed) {
          // if (x.isManadatory == "true") {
          //   this.errMsg = x.fieldName;
          //   isManadatory = false;
          // } else {
          //   isManadatory = true;
          // }         
          let list = {
            type: x.types,
            fieldName: x.fieldName,
            value: x.data || '',
            from: "",
            to: ""
          }
          if (x.types == "range") {
            list.from = x.from;
            list.to = x.to;
          }
          filData.push(list);
        } else {
          if (x.checked == true && x.typed == 4) {
            x.data.forEach(y => {
              let lisT = {
                type: y.type,
                fieldName: y.fieldName,
                value: y.value,
              }
              filData.push(lisT);
            });
          }
        }
      })
    }
    this.bannerShow = true;
    // $("html, body").animate({ scrollTop: 150 }, 500);
    let ltlg = {
      lat: $("#lati").val(),
      lng: $("#lngi").val()
    }
    if ($("#lati").val()) {
      this._conf.setItem('latlng', JSON.stringify(ltlg));
    }
    let ciSt = $("#cityAreai").val();
    if (ciSt) {
      this.recentCity = ciSt;
    }

    let lists = {
      token: this._conf.getItem('authToken'),
      offset: this.offset,
      limit: this.limit,
      location: "",
      latitude: "",
      longitude: "",
      currency: this.currency,
      postedWithin: this.postedWithin,
      sortBy: this.priceOrder,
      distance: this.distance,
      price:JSON.stringify({
        currency: this.currency,
        from : parseInt(this.minPrice),
        to: parseInt(this.maxPrice),

      }),
      langaugeCode: this.langaugeCode,
      categoryId:this.selectedCategoryNodeId,
      // minPrice: this.minPrice,
      // maxPrice: this.maxPrice,
      category: this.categoryName,
      // subCategory: this.subCategoryName,
      filter: JSON.stringify(filData),
    }
    let latlng = this._conf.getItem('latlng');
    console.log("the lat and lng is :",latlng )
    if (latlng) {
      let latlng = JSON.parse(this._conf.getItem('latlng'));
      lists.latitude = latlng.lat;
      lists.longitude = latlng.lng;
    }
    if (this.recentCity) {
      lists.location = this.recentCity;
    } else {
      lists.location = this._conf.getItem('recentCity')
    }
    if(this.subCategoryName){
      lists['subCategory'] = this.subCategoryName;
      lists['subcategoryId'] = this.subCategoryNodeId;
    } else{
      lists['subCategory'] = "";
    }
    // if (this.categoryName && isManadatory) {
    // if (this.offset == 0) {
    //   this.allListData = false;
    // }
    this.loaderButton = true;
    console.log("the search filters value :", lists)
    this._service.searchCategoriesList(lists, this.searchItem)
      .subscribe((res) => {
        this.loaderButton = false;
        // this.allListData = true;
        let allList = res.data;
        if (res.code == 200) {
          console.log("the allList for descending is 1:",this.allList );
          $("html, body").animate({ scrollTop: 0 }, 500);
          $("#sideDetailsId").removeClass('active');
          $("body").removeClass("hideHidden");
          // if(this.priceOrder == "priceDsc")
          // {
          //   console.log("the priceOrder from response is :", this.priceOrder);
          //   this.allList.sort((a, b) =>{return b.price - a.price});
          //   console.log("the decending :",this.allList );
          // }
          // else if(this.priceOrder == "priceAsc"){
          //   console.log("the priceOrder from response is :", this.priceOrder);
          //   this.allList.sort((a, b) =>{return a.price - b.price});
          //   console.log("the ascendeing :",this.allList );
          // }
          if (this.offset == 0) {
            this.allList = [];
          }
          for (var i = 0; i < allList.length; i++) {
            this.allList.push(res.data[i])
          }
          if (this.allList && this.allList.length > 0) {
            this.listsLength = false;
          } else {
            this.listsLength = true;
            // this.allListData = true;
          }
          this.listLength = this.allList.length;
          this.filterScroll = 1;
        } else {
          // $("html, body").animate({ scrollTop: 0 }, 500);
          // $("#sideDetailsId").toggleClass('active');
          // $("body").toggleClass("hideHidden");
          this.errMsg = res.message;
          this.listsLength = true;
          // this.allListData = true;
        }
      });
      // if(this.priceOrder == "priceDsc")
      // {
      //   console.log("the priceOrder from response is :", this.priceOrder);
      //   this.allList.sort((a, b) =>{return b.price - a.price});
      //   console.log("the decending :",this.allList );
      // }
      // else if(this.priceOrder == "priceAsc"){
      //   console.log("the priceOrder from response is :", this.priceOrder);
      //   this.allList.sort((a, b) =>{return a.price - b.price});
      //   console.log("the ascendeing :",this.allList );
      // }

      console.log("the allList for descending is :",this.allList);
      // this.searchItem = 0;
      // if($("#nameCategory").val() !== "" && typeof $("#nameCategory").val() !== "undefined"){
      //   this.cancelCats(1);
      // } 
    // } else {
    //   this.errMsg = "Mandatory field is missing";
    // }

  }

  setCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this.checkLocation();
      });
    }
  }


  checkLocation() {
    let latlng = this._conf.getItem("latlng");
    console.log(latlng)
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      $("#locationMap").modal("show");
      this.continueMap();
    }
  }

  changeLocation() {
    var input = document.getElementById('changeLocationId');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === 'administrative_area_level_1') {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
        }
      }
      // console.log("cityArea", City + ", " + state);
      if (City) {
        $("#cityAreai").val(City + ", " + state);
      } else if (state) {
        $("#cityAreai").val(state);
      } else {
        $("#cityAreai").val(place.formatted_address);
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      $("#lati").val(lat);
      $("#lngi").val(lng);
    });
  }

  locationChange1() {
    setTimeout(() => {
      this.offset = 0;
      this.searchCategory();
    }, 500);
  }


  locationChange() {
    setTimeout(() => {
      let lat = $("#lati").val();
      let lng = $("#lngi").val();
      this.latlng = {
        lat: lat,
        lng: lng
      }
      this.continueMap();
    }, 500);
  }


  continueMap() {
    var latlng = this.latlng;
    var map = new google.maps.Map(document.getElementById('mapData'), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      disableDefaultUI: false,
      mapTypeControl: false
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map,
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // console.log(results[0].formatted_address)
          $(".addressArea").val(results[0].formatted_address);
          $("#cityAreai").val(results[0].formatted_address);
          $(".location").text(results[0].formatted_address);
          // $(".areaName").val(results[0].formatted_address);
          // $(".arealat").val(latlng.lat);
          // $(".arealng").val(latlng.lng);
        }
      }
    });


    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', () => {

      geocoder.geocode({ 'latLng': marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            $("#lati").val(marker.getPosition().lat());
            $("#lngi").val(marker.getPosition().lng());
            $(".location").text(results[0].formatted_address);
            $("#cityAreai").val(results[0].formatted_address);
            $(".addressArea").val(results[0].formatted_address);
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
          }
        }
      });
    });
  }
  addCommaToPrice(val){
    // console.log("the price string is: 2", val)
    var str = val.toString().split('.');
    // console.log("the price string is 1:", str)
    if (str[0].length >= 4) {
        // str[0] = str[0].match(/.{3}/g).join('.');
        str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    if (str[1] && str[1].length >= 2) {
      str[1] = str[1].substr(0,2);  
        // str[1] = str[1].replace(/(\d{2})/g, '$1 ');
    }
    // console.log("the price string is:", str)
    return str.join(',');
  }

}
