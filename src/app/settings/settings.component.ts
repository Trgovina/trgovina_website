import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { RouterLinkActive } from '@angular/router';
import { MissionService } from '../app.service';
import { SettingsService } from './settings.service';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angularx-social-login";

declare var $: any;
declare var google: any;
declare var FB: any;
declare var moment: any;

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {


  headerOpen: string;
  headerRefresh: string;
  token: any;
  userName: any;
  email: any;
  profilePicUrl: any;
  phone_Error = false;
  email_Error = false;
  registerSave = false;
  editData: any;
  errorMsg: any;
  loaderButton = true;
  cloudinaryImage: any;
  follow_count = 0;
  cloudinaryOptions: CloudinaryOptions = new CloudinaryOptions({
    cloudName: 'yeloadmin',
    uploadPreset: 'iirvrtog',
    autoUpload: true
  });

  uploader: CloudinaryUploader = new CloudinaryUploader(this.cloudinaryOptions);

  constructor(
    private _missionService: MissionService,
    private _conf: Configuration,
    private _router: Router,
    private _zone: NgZone,
    private _service: SettingsService,
    public _auth: AuthService,
    private _lang: LanguageService) {

    _missionService.missionheaderRefresh$.subscribe(
      headerRefresh => {
        this.ngOnInit();
      });
  }


  sellingOffer: any;
  offset = 0;
  limit = 40;
  internetModal = false;


  buyingOffer: any;
  loginUsername: any;
  watchOffer: any;


  offerListLength = false;
  allListProfile = false;
  following: any;
  followingErr: any;
  followerLength: any;
  followingLength: any;
  postLength: any;
  tileFollowers: any;
  loaderButtonPopup = false;
  loaderButtonProfile = false;
  settings: any;
  emptyStar = [];
  noPostContent = 0;
  countryCode:any ='';


  sub: any;
  accessToken: any;
  payPal = "https://www.paypal.me/";

  ngOnInit() {

    // this.settings = this._lang.engSettings;
    let selectedLang  = Number(sessionStorage.getItem("Language"));
    if(selectedLang){
      switch(selectedLang){

        case 1: case 2:  this.settings = this._lang.engSettings1;
                          break;

        case 3:  this.settings = this._lang.engSettings;
                break;
      }
    }
    else{
      this.settings = this._lang.engSettings1;
    }

    $(window).on('popstate', function () {
      $('.modal').modal('hide');
    });

    this._missionService.confirmheaderOpen(this.headerOpen);
    this.token = this._conf.getItem('authToken');
    if (!this.token) {
      this._router.navigate([""]);
    }

    this.userName = this._conf.getItem('username');
    this.email = this._conf.getItem('email');
    this.profilePicUrl = this._conf.getItem('profilePicUrl');

    setTimeout(() => {
      this.allListProfile = true;
    }, 500);
    this.allListProcess(0);
    this.getFollowing();
    this.myeditProfile();
    this.getFollowers();
    this.emptyFields();

    // var input = document.getElementById('location');
    // var autocomplete = new google.maps.places.Autocomplete(input);

    // autocomplete.addListener('place_changed', function () {
    //   var place = autocomplete.getPlace();
    //   let lati = place.geometry.location.lat();
    //   let lngi = place.geometry.location.lng();
    //   var place = $(".place").val(place.formatted_address);
    //   var lat = $(".lat").val(lati);
    //   var lng = $(".lng").val(lngi);
    // });

    // $("#mobileSignin").intlTelInput({
    //   nationalMode: true,
    //   separateDialCode: true,
    //   initialCountry: "auto",
    //   geoIpLookup: function (callback) {
    //     $.get('//ipapi.co/json', function () { }, "jsonp").always(function (resp) {

    //       var countryCode = (resp && resp.country) ? resp.country : "";
    //       this.countryCode = resp.country;
    //       callback(countryCode);
    //     });
    //   },
    //   autoPlaceholder: false,
    //   // modify the auto placeholder
    //   customPlaceholder: null,
    //   // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    // });
  }
  EditProfile(){
    // for google autoComplete
    var input = document.getElementById('location');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      let lati = place.geometry.location.lat();
      let lngi = place.geometry.location.lng();
      var place = $(".place").val(place.formatted_address);
      var lat = $(".lat").val(lati);
      var lng = $(".lng").val(lngi);
    });
    // for intl-input 
    
      // $.get('https://ipapi.co/json', function () { }).always(function (resp) {

      //   // var countryCode = (resp && resp.country) ? resp.country : "";
      //   this.countryCode = resp.country;
      //   this._conf.setItem('country',resp.country )
      // })
    setTimeout(()=>{$("#mobileSignin").intlTelInput({
      nationalMode: true,
      separateDialCode: true,
      initialCountry: this._conf.getItem('country' ),
      autoPlaceholder: false,
      // modify the auto placeholder
      customPlaceholder: null,
      // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
  },1000);
  }
  public emptyStars = [
    { star: false },
    { star: false },
    { star: false },
    { star: false },
    { star: false }
  ]

  emptyFields() {
    this.emptyStar = [];
    for (var i = 0; i < this.emptyStars.length; i++) {
      this.emptyStar.push(this.emptyStars[i]);
    }
    // console.log(this.emptyStar)
  }

  starActive(val: number) {
    for (var i = 0; i < val; i++) {
      this.emptyStar[i].star = true;
    }
    // console.log(this.emptyStar)
  }

  allListProcess(val) {
    this.noPostContent = val;
    let list = {
      token: this.token,
      offset: this.offset,
      limit: this.limit
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 3000);
    this._service.sellingPostList(list, val)
      .subscribe((res) => {
        if (res.code == 200) {
          this.loaderButton = false;
          this.sellingOffer = res.data;
          if (val == 3) {
            this.timeFunction();
          }
          if (res.data && res.data.length > 0) {
            this.offerListLength = true;
            this.postLength = res.data.length;
          } else {
            this.offerListLength = false;
            this.postLength = 0;
          }
        } else {
          this.offerListLength = false;
          this.postLength = 0;
          this.loaderButton = false;
        }
      });
  }


  timeFunction() {
    var len = this.sellingOffer;
    for (var i = 0; i < len.length; i++) {
      var date = moment(this.sellingOffer[i].acceptedOn).valueOf();
      var post = date;
      var poston = Number(post);
      var postdate = new Date(poston);
      var currentdate = new Date();
      var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
      var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);

      if (1 < diffDays) {
        this.sellingOffer[i].acceptedDate = diffDays + " d";
      } else if (1 <= diffHrs) {
        this.sellingOffer[i].acceptedDate = diffHrs + " h";
      } else {
        this.sellingOffer[i].acceptedDate = diffMins + " m";
      }
    }
  }

  buildTrusts() {
    $("#buildTrustPopup").modal("show");
  }

  getFollowers() {
    let list = {
      token: this.token,
      offset: this.offset,
      limit: this.limit
    }
    this.tileFollowers = "Follower";
    this.following = [];
    this.loaderButtonPopup = true;
    setTimeout(() => {
      this.loaderButtonPopup = false;
    }, 3000);
    this._service.followerPostList(list)
      .subscribe((res) => {
        this.loaderButtonPopup = false;
        if (res.code == 200) {
          this.followingErr = false;
          this.following = res.followers;
          if (res.followers && res.followers.length) {
            this.followerLength = res.followers.length;
          } else {
            this.followerLength = 0;
          }
        } else {
          this.followerLength = 0;
          this.followingErr = res.message;
        }
      });

  }


  getFollowing() {
    let list = {
      token: this.token,
      offset: this.offset,
      limit: this.limit
    }
    this.tileFollowers = "Following";
    this.following = [];
    this.loaderButtonPopup = true;
    setTimeout(() => {
      this.loaderButtonPopup = false;
    }, 3000);
    this._service.followingPostList(list)
      .subscribe((res) => {
        this.loaderButtonPopup = false;
        if (res.code == 200) {
          this.followingErr = false;
          this.following = res.result;
          if (res.result && res.result.length) {
            this.followingLength = res.result.length;
          } else {
            this.followingLength = 0;
          }
        } else {
          this.followingLength = 0;
          this.followingErr = res.message;
        }
      });
  }


  followerButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        userNameToFollow: val
      }
      this.following[i].loaderFlag = true;
      setTimeout(() => {
        this.following[i].loaderFlag = false;
      }, 3000);
      this.following[i].userFollowRequestStatus = 1;
      this._service.followersPostListButton(list)
        .subscribe((res) => {
          this.following[i].loaderFlag = false;

        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }

  unFollowButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        unfollowUserName: val
      }
      this.following[i].loaderFlag = true;
      setTimeout(() => {
        this.following[i].loaderFlag = false;
      }, 3000);
      this.following[i].userFollowRequestStatus = null;
      this._service.unfollowPostListButton(list)
        .subscribe((res) => {
          this.following[i].loaderFlag = false;
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }



  editProfile = {
    profilePicUrl: '',
    fullName: '',
    username: '',
    bio: '',
    websiteUrl: '',
    email: '',
    phoneNumber: '',
    place: '',
    latitude: '',
    longitude: '',
    facebookVerified: '',
    googleVerified: '',
    paypalUrl:''
  }

  myeditProfile() {
    let list = {
      token: this.token,
    }
    this.errorMsg = false;
    this._service.editProfile(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.editProfile = res.data;
          if (res.data.avgRating) {
            this.starActive(res.data.avgRating);
          }
        } else {
          this.errorMsg = res.message;
        }
      });
  }

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.saveProfile();
    }
  }


  saveProfile() {
    let flag = $("#mobileSignin").intlTelInput("getSelectedCountryData");
    var place = $(".place").val();
    var lat = $(".lat").val();
    var lng = $(".lng").val();
    var image = $(".settingImg").attr("src");
    if (image != '../assets/images/default-thumnail.png') {
      console.log("not image", image);
      this.editProfile.profilePicUrl = image;
    }
    this.editProfile.place = place;
    this.editProfile.latitude = lat;
    this.editProfile.longitude = lng;
    if (this.editProfile.phoneNumber.charAt(0) != '+') {
      this.editProfile.phoneNumber = "+" + flag.dialCode + this.editProfile.phoneNumber;
    }
    this.loaderButtonProfile = true;
    setTimeout(() => {
      this.loaderButtonProfile = false;
    }, 3000);
    // console.log(this.editProfile);
    this._service.saveProfilePost(this.editProfile)
      .subscribe((res) => {
        if (res.code == 200) {
          this.loaderButtonProfile = false;
          this._conf.setItem('authToken', res.token);
          this._conf.setItem('profilePicUrl', image);
          this._conf.setItem('username', this.editProfile.username);
          // this.editData = res.data; 
          this.errorMsg = "Success";
          this._missionService.confirmheaderRefresh(this.headerRefresh);
          setTimeout(() => {
            this.errorMsg = false;
            $(".modal").modal("hide");
          }, 3000);
        } else {
          this.errorMsg = res.message;
          setTimeout(() => {
            this.errorMsg = false;
          }, 3000);
        }
      });
  }

  fileUploader() {
    this.errorMsg = false;
    $(".imgCloudinary").show();
    this.uploader.uploadAll();
    setTimeout(() => {
      this.uploader.onSuccessItem = function (item: any, response: any, status: number, headers: any) {
        this.cloudinaryImage = JSON.parse(response);
        $(".settingImg").attr("src", this.cloudinaryImage.url);
        $(".imgCloudinary").hide();
        console.log(this.cloudinaryImage.url);
        return { item, response, status, headers };
      };
    }, 200);
  }

  changePassword() {
    $(".modal").modal("hide");
    this._router.navigate(['./password']);
  }

  emailClick() {
    this._router.navigate(['./emailVerify']);
  }

  itemDetails(postId) {
    this._router.navigate(["./item", postId]);
  }

  // onFacebookLoginClick() {
  //   this.sub = this._auth.login("facebook").subscribe(
  //     (result) => {
  //       // console.log(result);
  //       this.me();
  //     }
  //   )
    
  // }
  signInWithGoogle(): void {
    this._auth.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => 
      {
        console.log("the google user",x);
        this.onGoogle();
      });
    // this._auth.signIn(GoogleLoginProvider.PROVIDER_ID);
    // this.onGoogle();
  }

  signInWithFB(): void {
    this._auth.signIn(FacebookLoginProvider.PROVIDER_ID).then(x =>
       {
         console.log("the facebook user ",x); 
         this. facebookVerify(x);
        });
    // this._auth.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  // me(value) {
  //   FB.api('/me?fields=id,picture,name,first_name,email,gender',
  //     (result: any) => {
  //       this.facebookVerify(value);
  //     })
  // }


  facebookVerify(value) {
    // FB.getLoginStatus((response: any) => {
    //   if (response) {
    //     var accessToken = response.authResponse.accessToken;
        // console.log("test", accessToken);
        let list = {
          token: this.token,
          accessToken: value.authToken
        }
        console.log("the list :", list)
        this._service.facebookPost(list)
          .subscribe((res) => {
            if (res.code == 200) {
              this.editProfile.facebookVerified == "1";
              var currentLocation = window.location;
              this._conf.setItem("pathname", currentLocation.pathname);
              this._router.navigate(['']);
              window.location.replace('');  
            } else {
              $("#verified").modal("show");
              $(".errorMsgs").text(res.message);
            }
          });
    //   }
    // });
  }

  // onGoogleLoginClick() {
  //   this.sub = this._auth.login("google").subscribe(
  //     (googleUser) => {
  //       console.log(googleUser);
  //       this.onSuccess(googleUser);
  //     }
  //   )
  // }

  onGoogle() {
    // console.log("the test for google login", googleUser)
    // 
      this._auth.authState.subscribe((user) => {        
        console.log("test", user);
        if (user.provider == 'GOOGLE') {
          this._zone.run(() => {
          var googleUser = user;
      let list = {
        googleId: googleUser.id,
        accessToken: googleUser.idToken,
        token: this.token,
      }
      this._service.googlePost(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.editProfile.googleVerified == "1";
            var currentLocation = window.location;
            this._conf.setItem("pathname", currentLocation.pathname);
            this._router.navigate(['']);
            window.location.replace('');
          } else {
            $("#verified").modal("show");
            $(".errorMsgs").text(res.message);
          }
        });
      });
    }
    });
  }
  payPalList() {
    let list = {
      paypalUrl: this.payPal,
      token: this.token,
    }
    this.loaderButtonProfile = true;
    this._service.paypalPost(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.editProfile.paypalUrl == "1";
          $(".modal").modal("hide");
          this.loaderButtonProfile = false;
        }
      });
  }
  addCommaToPrice(val){
    let str ;
    // console.log("the price is:",val )
    // if(val.toString().lastIndexOf('.') <= -1){
    //   str = val.toString();
    //   console.log("the lastIndexOf", str)
    // }
    str = val.toString().split('.');
    if (str[0].length >= 4) {
        str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        // str[0] = str[0].match(/.{1,3}/g).join('.');
        // str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        // let valu1 = str[0].split('.');
        // if(valu1.length == 2 ){
        //   str[0] =  valu1[0]+","+valu1[1];
        // } 
    }
    if (str[1] && str[1].length >= 2) {
      str[1] = str[1].substr(0,2);        // str[0] = str[0].match(/.{1,3}/g).join('.');
        // str[1] = str[1].replace(/(\d{2})/g, '$1 ');
    }
    return str.join(',');
  }


}


