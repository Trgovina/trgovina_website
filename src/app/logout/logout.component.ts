import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MissionService } from '../app.service';
import { HomeService } from '../home/home.service';
import { LanguageService } from '../app.language';
import { Configuration } from '../app.constants';

@Component({
  selector: 'logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private _router: Router, 
    private _missionService: MissionService,  
    private _service: HomeService,
    private _conf: Configuration,
    private _lang: LanguageService) { }

  headerClosed: string;
  allList:any;
  catList:any;
  logout:any
  languageCode:string ='';
  ngOnInit() {

    // this.logout = this._lang.engLogout;
    // let selectedLang  = Number(this._conf.getItem("Language"));
    let selectedLang  = Number(sessionStorage.getItem("Language"));
    if(selectedLang){
      switch(selectedLang){

        case 1: this.logout = this._lang.engLogout1;
                this.languageCode= 'rs';
                break;
        case 2: this.logout = this._lang.engLogout1;
                this.languageCode= 'me';
                break;

        case 3: this.logout = this._lang.engLogout;
                this.languageCode= 'en';
                break;
      }
    }
    else{
      this.logout = this._lang.engLogout1;
      this.languageCode= 'rs';
    }

    this._missionService.confirmheaderClosed(this.headerClosed);
    // this._router.navigate([""]);
    this.listGetCategories();

     let list = {
      offset: 0,
      limit: 40
    }
    this._service.homeAllList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.allList = res.data;          
          console.log('length', this.allList.length);
        }
      });
  }

  listGetCategories() {
    this._service.getCategoriesList(this.languageCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
        }
      });
  }

}
